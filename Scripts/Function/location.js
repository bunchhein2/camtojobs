﻿$(document).ready(function () {
    $('#locationModal').on('show.bs.modal', function () {
        GetLocations();
    });
});
var tableLocations = [];
function GetLocations() {
    tableLocations = $('#locationTbl').dataTable({
        ajax: {
            url: "/api/Locations",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id",
                },
                {
                    data: "location",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='LocationsEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='LocationsDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function LocationAction() {
    var action = '';
    action = document.getElementById('btnLocation').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnLocation').innerText = 'Save';
        EnableControlLo();
        ClearControlLo();
        $('#locationname').focus();
    }

    if (action === "Save") {
        //Validate();
        var data = {
            location: $('#locationname').val(),
            price: $('#price').val(),
        };
        $.ajax({
            url: "/api/Locations",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#locationTbl').DataTable().ajax.reload();

                //$('#PositionModal').modal('hide');
                ClearControlLo();
                DisableControlLo();
                document.getElementById('locationTbl').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#ID').val(),
            location: $('#locationname').val(),
            price: $('#price').val(),
        };
        $.ajax({
            url: "/api/Locations/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#locationTbl').DataTable().ajax.reload();


                ClearControlLo();
                DisableControlLo();
                document.getElementById('btnLocation').innerText = 'Add New';

                //$('#PositionModal').modal('hide');
                //tablePositions.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}


function LocationsEdit(id) {
    //ClearControlSh();
    EnableControlLo();
    action = document.getElementById('btnLocation').innerText = "Update";

    $.ajax({
        url: "/api/Locations/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            $('#locationname').val(result.location);
            $('#price').val(result.price);


        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function LocationsDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Locations/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#locationTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControlLo() {
    document.getElementById('locationname').disabled = true;
    document.getElementById('price').disabled = true;
}

function EnableControlLo() {
    document.getElementById('locationname').disabled = false;
    document.getElementById('price').disabled = false;
}

function ClearControlLo() {
    $('#locationname').val('');
    $('#price').val('0.00');
}

function AddNewLocationAction() {
    document.getElementById('btnLocation').innerText = "Add New";
    DisableControlLo();
    ClearControlLo();
}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#locationname').val().trim() === "") {
        $('#locationname').css('border-color', 'red');
        $('#locationname').focus();
        toastr.info("Please enter Position Name", "Required");
    }
    else {
        $('#locationname').css('border-color', '#cccccc');
    }
    return isValid;
}