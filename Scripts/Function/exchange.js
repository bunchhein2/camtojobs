﻿$(document).ready(function () {
    $('#exchangeModal').on('show.bs.modal', function () {
        GetExchange();
    });
});
var tableExchanges = [];
function GetExchange() {
    tableExchanges = $('#exchangeTbl').dataTable({
        ajax: {
            url: "/api/Exchanges",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "rateid",
                },
                {
                    data: "rate",
                },
                {
                    data: "rateid",
                    render: function (data) {
                        return "<button OnClick='ExchangeEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='ExchangeDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function ExchangeAction() {
    var action = '';
    action = document.getElementById('btnExchange').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnExchange').innerText = 'Save';
        EnableControlEx();
        ClearControlEx();
        $('#exchange').focus();
        $('#eDate').val(moment().format('YYYY-MM-DD'));
    }

    if (action === "Save") {
        //Validate();
        var data = {
            rate: $('#exchange').val(),
            //date: $('#eDate').val(),
        };
        $.ajax({
            url: "/api/Exchanges",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#exchangeTbl').DataTable().ajax.reload();

                ClearControlEx();
                DisableControlEx();
                document.getElementById('btnExchange').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        var data = {
            rateid: $('#ID').val(),
            rate: $('#exchange').val(),
            //date: $('#eDate').val(),
        };
        $.ajax({
            url: "/api/Exchanges/" + data.rateid,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#exchangeTbl').DataTable().ajax.reload();


                ClearControlEx();
                DisableControlEx();
                document.getElementById('btnExchange').innerText = 'Add New';

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}


function ExchangeEdit(id) {
    //ClearControlSh();
    EnableControlEx();
    action = document.getElementById('btnExchange').innerText = "Update";

    $.ajax({
        url: "/api/Exchanges/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.rateid);
            $('#exchange').val(result.rate);
            //var dr = moment(result.date).format("YYYY-MM-DD");
            //$('#eDate').val(dr);
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function ExchangeDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Exchanges/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#exchangeTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlEx() {
    document.getElementById('exchange').disabled = true;
}

function EnableControlEx() {
    document.getElementById('exchange').disabled = false;
}

function ClearControlEx() {
    $('#exchange').val('');
}

function AddNewExchangeAction() {
    document.getElementById('btnExchange').innerText = "Add New";
    DisableControlEx();
    ClearControlEx();
}

