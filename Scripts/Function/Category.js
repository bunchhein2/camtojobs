﻿$(document).ready(function () {
    $('#CategoryModal').on('show.bs.modal', function () {
        GetCategory();
        //alert('hi');        
    });
});


var tableCategorys = [];
function GetCategory() {
    tableCategory = $('#tblCategory').dataTable({
        ajax: {
            url: "/api/Categories",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "categoryName",
                },                
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CategoryEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='CategoryDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}
function CategoryAction() {
    var action = '';
    action = document.getElementById('btnSaveCategory').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnSaveCategory').innerText = 'Save';
        EnableControl();
        $('#CategoryName').focus();
    }

    if (action === "Save") {
        var data = {
            categoryName: $('#CategoryName').val(),
        };
        $.ajax({
            url: "/api/Categories",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#tblCategory').DataTable().ajax.reload();

                //$('#CategoryModal').modal('hide');
                ClearControl();
                DisableControl();
                document.getElementById('btnSaveCategory').innerText = 'Add New';

                //document.getElementById('btnSaveCategory').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This Category is already exists.", "Server Response");
                //document.getElementById('btnSaveCategory').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#CategoryID').val(),
            categoryName: $('#CategoryName').val(),
        };
        $.ajax({
            url: "/api/Categories/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#tblCategory').DataTable().ajax.reload();

                ClearControl();
                DisableControl();
                document.getElementById('btnSaveCategory').innerText = 'Add New';
                
                //$('#CategoryModal').modal('hide');
                //tableCategorys.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Category can't Update.", "Server Response");

            }
        });
    }
}


function CategoryEdit(id) {
    ClearControl();
    EnableControl();
    //$('#status').val('');
    action = document.getElementById('btnSaveCategory').innerText = "Update";

    $.ajax({
        url: "/api/Categories/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#CategoryID').val(result.id);
            $('#CategoryName').val(result.categoryName);
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function CategoryDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Categories/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblCategory').DataTable().ajax.reload();

                        toastr.success("Category Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Category Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControl() {
    document.getElementById('CategoryName').disabled = true;
}

function EnableControl() {

    document.getElementById('CategoryName').disabled = false;
}

function ClearControl() {
    $('#CategoryName').val('');
}

function AddnewCategoryAction() {
    document.getElementById('btnSaveCategory').innerText = "Add New";
    DisableControl();
    ClearControl();
}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#CategoryName').val().trim() === "") {
        $('#CategoryName').css('border-color', 'red');
        $('#CategoryName').focus();
        toastr.info("Please enter Category Name", "Required");
    }
    else {
        $('#CategoryName').css('border-color', '#cccccc');
        if ($('#createBy').val().trim() === "") {
            $('#createBy').css('border-color', 'red');
            $('#createBy').focus();
            toastr.info("Please enter your phone", "Required");
        }
        else {
            $('#createBy').css('border-color', '#cccccc');
        }
    }
    return isValid;
}