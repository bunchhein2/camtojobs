﻿$(document).ready(function () {
    $('#positionModal').on('show.bs.modal', function () {
        GetPosition();       
    });
});
var tablePositions = [];
function GetPosition() {
    tablePosition = $('#Positiontbl').dataTable({
        ajax: {
            url: "/api/Positions",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "positionName",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='PositionEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='PositionDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}
function PositionAction() {
    var action = '';
    action = document.getElementById('btnPosition').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnPosition').innerText = 'Save';
        EnableControl();
        $('#positionName').focus();
    }

    if (action === "Save") {
        Validate();
        var data = {
            positionName: $('#positionName').val(),
        };
        $.ajax({
            url: "/api/Positions",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#Positiontbl').DataTable().ajax.reload();

                //$('#PositionModal').modal('hide');
                ClearControl();
                DisableControl();
                document.getElementById('btnPosition').innerText = 'Add New';

                //document.getElementById('btnSavePosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
                //document.getElementById('btnSavePosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#ID').val(),
            positionName: $('#positionName').val(),
        };
        $.ajax({
            url: "/api/Positions/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#Positiontbl').DataTable().ajax.reload();

                ClearControl();
                DisableControl();
                document.getElementById('btnPosition').innerText = 'Add New';

                //$('#PositionModal').modal('hide');
                //tablePositions.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}


function PositionEdit(id) {
    ClearControl();
    EnableControl();
    //$('#status').val('');
    action = document.getElementById('btnPosition').innerText = "Update";

    $.ajax({
        url: "/api/Positions/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            $('#positionName').val(result.positionName);
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function PositionDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Positions/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#Positiontbl').DataTable().ajax.reload();

                        toastr.success("Position Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Position Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControl() {
    document.getElementById('positionName').disabled = true;
}

function EnableControl() {

    document.getElementById('positionName').disabled = false;
}

function ClearControl() {
    $('#positionName').val('');
}

function AddnewPositionAction() {
    document.getElementById('btnPosition').innerText = "Add New";
    DisableControl();
    ClearControl();
}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#positionName').val().trim() === "") {
        $('#positionName').css('border-color', 'red');
        $('#positionName').focus();
        toastr.info("Please enter Position Name", "Required");
    }
    else {
        $('#positionName').css('border-color', '#cccccc');
    }
    return isValid;
}