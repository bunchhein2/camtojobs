﻿$(document).ready(function () {
    $('#productTypeModal').on('show.bs.modal', function () {
        GetProductTypes();
    });
});
var tableProductTypes = [];
function GetProductTypes() {
    tableProductTypes = $('#productTypeTbl').dataTable({
        ajax: {
            url: "/api/ProductTypes",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "producttypename",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ProductTypesEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='ProductTypesDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function ProductTypeAction() {
    var action = '';
    action = document.getElementById('btnProductType').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnProductType').innerText = 'Save';
        EnableControlProT();
        ClearControlProT();
        $('#proName').focus();
    }

    if (action === "Save") {
        //Validate();
        var data = {
            producttypename: $('#proName').val(),
        };
        $.ajax({
            url: "/api/ProductTypes",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#productTypeTbl').DataTable().ajax.reload();

                //$('#PositionModal').modal('hide');
                ClearControlProT();
                DisableControlProT();
                document.getElementById('btnProductType').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#ID').val(),
            producttypename: $('#proName').val(),
        };
        $.ajax({
            url: "/api/ProductTypes/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#productTypeTbl').DataTable().ajax.reload();


                ClearControlProT();
                DisableControlProT();
                document.getElementById('btnProductType').innerText = 'Add New';

                //$('#PositionModal').modal('hide');
                //tablePositions.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}


function ProductTypesEdit(id) {
    //ClearControlSh();
    EnableControlProT();
    action = document.getElementById('btnProductType').innerText = "Update";

    $.ajax({
        url: "/api/ProductTypes/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            $('#proName').val(result.producttypename);


        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function ProductTypesDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ProductTypes/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#productTypeTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControlProT() {
    document.getElementById('proName').disabled = true;

}

function EnableControlProT() {
    document.getElementById('proName').disabled = false;
}

function ClearControlProT() {
    $('#proName').val('');
}

function AddNewProductTypeAction() {
    document.getElementById('btnProductType').innerText = "Add New";
    DisableControlProT();
    ClearControlProT();
}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#proName').val().trim() === "") {
        $('#proName').css('border-color', 'red');
        $('#proName').focus();
        toastr.info("Please enter Position Name", "Required");
    }
    else {
        $('#proName').css('border-color', '#cccccc');
    }
    return isValid;
}