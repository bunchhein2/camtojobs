﻿$(document).ready(function () {
    GetCustomer('All');
    $('#displayShowRoom').on('change', function () {
        var showRoomId = this.value;
        if (showRoomId == '-----Select ShowRoom-----') {
            GetCustomer('All');
        } else {
            GetCustomer(showRoomId);
        }
    })
});
var tableCustomer = [];
function GetCustomer(showRoomId) {
    tableCustomer = $('#customerTbl').DataTable({
        ajax: {
            url: (showRoomId == 'all') ? "/api/Customers?showRoomId=all" : "/api/Customers?showRoomId=" + showRoomId,
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "name",
                },
                {
                    data: "sex",
                },
                {
                    data: "phone",
                },
                {
                    data: "address",
                },
                {
                    data: "customertype",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CustomerEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='CustomerDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function readURLCustomer(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#customerPhoto").attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
};
function CustomerAction() {
    var action = '';
    action = document.getElementById('btnCus').innerText;

    if (action == "Add New") {
        document.getElementById('btnCus').innerText = 'Save';
        EnableControlCU();
        ClearControlCU();
        $('#name').focus();
        //$('#dob').val(moment().format('YYYY-MM-DD'));
        $('#customerPhoto').attr('src', '../Images/company.png');
    }
    else if (action == "Save") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("photo", files[0]);
        }

        data.append("name", $('#name').val());
        data.append("sex", $('#sex').val());
        data.append("phone", $('#phoneC').val());
        data.append("address", $('#address').val());
        data.append("showroomid", $('#showroomid').val());
        data.append("idenityNo", $('#identityNo').val());
        data.append("customertype", $('#customerType').val());



        $.ajax({
            url: "/api/Customers",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Save To Database Successfully", "Server Respond");
                $('#customerTbl').DataTable().ajax.reload();
                document.getElementById('btnCus').innerText = 'Add New';
                DisableControlCU();
                $("#customersModal").modal('hide');
            },
            error: function (errormesage) {
                $('#name').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("photo", files[0]);
        }

        data.append("id", $('#id').val());

        data.append("name", $('#name').val());
        data.append("sex", $('#sex').val());
        data.append("phone", $('#phoneC').val());
        data.append("address", $('#address').val());
        data.append("showroomid", $('#showroomid').val());
        data.append("idenityNo", $('#identityNo').val());
        data.append("customertype", $('#customerType').val());
        data.append("file_old", $("#file_old").val());

        $.ajax({
            url: "/api/Customers?id=" + $("#id").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Data has been Updated", "Server Respond");
                $('#customerTbl').DataTable().ajax.reload();
                document.getElementById('btnCus').innerText = 'Add New';
                DisableControlCU();
                $("#customersModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Data hasn't Updated in Database", "Server Respond")
            }
        });
    }
}
function CustomerEdit(id) {
    //alert('hi');
    ClearControlCU();
    EnableControlCU();
    action = document.getElementById('btnCus').innerText = "Update";

    $.ajax({
        url: "/api/Customers?id=" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#id').val(result.id);

            ///console.log(result.id);
            //var dr = moment(result.dob).format("YYYY-MM-DD");
            //$("#dob").val(dr);

            $("#name").val(result.name);
            $("#sex").val(result.sex);
            $("#address").val(result.address);
            $("#showroomid").val(result.showroomid);
            $("#phoneC").val(result.phone);
            $("#idenityNo").val(result.identityno);
            $("#customerType").val(result.customertype);
            $('#file_old').val(result.photo);

            if (result.photo == "") {
                $("#customerPhoto").attr('src', '../Images/company.png');
            } else {
                $("#customerPhoto").attr('src', '../Images/' + result.photo);
            }

            $("#customersModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}
function CustomerDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Customers/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#customerTbl').DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControlCU() {
    document.getElementById('name').disabled = true;
    document.getElementById('sex').disabled = true;
    document.getElementById('address').disabled = true;
    document.getElementById('phoneC').disabled = true;
    document.getElementById('idenityNo').disabled = true;
    document.getElementById('customerType').disabled = true;
    document.getElementById('showroomid').disabled = true;
}

function EnableControlCU() {
    document.getElementById('name').disabled = false;
    document.getElementById('sex').disabled = false;
    document.getElementById('address').disabled = false;
    document.getElementById('phoneC').disabled = false;
    document.getElementById('idenityNo').disabled = false;
    document.getElementById('customerType').disabled = false;
    document.getElementById('showroomid').disabled = false;

}

function ClearControlCU() {
    $('#name').val('');
    $('#sex').val('');
    $('#address').val('');
    $('#phoneC').val('');
    $('#idenityNo').val('');
    $('#customerType').val('');
    $('#showroomid').val('');
    $('#customerPhoto').attr('src', '../Images/company.png');
}

function AddNewCustomerAction() {
    document.getElementById('btnCus').innerText = "Add New";
    DisableControlCU();
    ClearControlCU();
    $('#customerPhoto').attr('src', '../Images/company.png');
}
