﻿$(document).ready(function () {
    $('#showRoomModal').on('show.bs.modal', function () {
        GetShowRoom();
    });
});
var tableShowRooms = [];
function GetShowRoom() {
    tableShowRooms = $('#ShowRoomTbl').dataTable({
        ajax: {
            url: "/api/ShowRooms",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "name",
                },
                {
                    data: "address",
                },
                {
                    data: "owner",
                },
                {
                    data: "phone",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ShowRoomEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='ShowRoomDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function ShowRoomAction() {
    var action = '';
    action = document.getElementById('btnShowRoom').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnShowRoom').innerText = 'Save';
        EnableControlSh();
        ClearControlSh();
        $('#showRoomName').focus();
    }

    if (action === "Save") {
        //Validate();
        var data = {
            name: $('#showRoomName').val(),
            address: $('#address').val(),
            owner: $('#ownerName').val(),
            phone: $('#phoneNumber').val(),
        };
        $.ajax({
            url: "/api/ShowRooms",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#ShowRoomTbl').DataTable().ajax.reload();

                //$('#PositionModal').modal('hide');
                ClearControlSh();
                DisableControlSh();
                document.getElementById('btnShowRoom').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        var data = {
            id: $('#ID').val(),
            name: $('#showRoomName').val(),
            address: $('#address').val(),
            owner: $('#ownerName').val(),
            phone: $('#phoneNumber').val(),
        };
        $.ajax({
            url: "/api/ShowRooms/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#ShowRoomTbl').DataTable().ajax.reload();


                ClearControlSh();
                DisableControlSh();
                document.getElementById('btnShowRoom').innerText = 'Add New';

                //$('#PositionModal').modal('hide');
                //tablePositions.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}


function ShowRoomEdit(id) {
    //ClearControlSh();
    EnableControlSh();
    action = document.getElementById('btnShowRoom').innerText = "Update";

    $.ajax({
        url: "/api/ShowRooms/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            $('#showRoomName').val(result.name);
            $('#address').val(result.address);
            $('#ownerName').val(result.owner);
            $('#phoneNumber').val(result.phone);


        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function ShowRoomDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/ShowRooms/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#ShowRoomTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}



function DisableControlSh() {
    document.getElementById('showRoomName').disabled = true;
    document.getElementById('address').disabled = true;
    document.getElementById('ownerName').disabled = true;
    document.getElementById('phoneNumber').disabled = true;

}

function EnableControlSh() {
    document.getElementById('showRoomName').disabled = false;
    document.getElementById('address').disabled = false;
    document.getElementById('ownerName').disabled = false;
    document.getElementById('phoneNumber').disabled = false;
}

function ClearControlSh() {
    $('#showRoomName').val('');
    $('#address').val('');
    $('#ownerName').val('');
    $('#phoneNumber').val('');
}

function ShowRoomerModalAction() {
    document.getElementById('btnShowRoom').innerText = "Add New";
    DisableControlSh();
    ClearControlSh();
}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#showRoomName').val().trim() === "") {
        $('#showRoomName').css('border-color', 'red');
        $('#showRoomName').focus();
        toastr.info("Please enter Position Name", "Required");
    }
    else {
        $('#showRoomName').css('border-color', '#cccccc');
    }
    return isValid;
}