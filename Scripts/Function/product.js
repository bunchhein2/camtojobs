﻿$(document).ready(function () {
    GetProduct('All');
    $('#displayShowRoom').on('change', function () {
        var showRoomId = this.value;
        if (showRoomId == '-----Select ShowRoom-----') {
            GetProduct('All');
        } else {
            GetProduct(showRoomId);
        }
    })
});
var tableProduct = [];
function GetProduct(showRoomId) {
    tabletProduct = $('#productTbl').DataTable({
        ajax: {
            url: (showRoomId == 'all') ? "/api/Products?showRoomId=all" : "/api/Products?showRoomId=" + showRoomId,
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "productname",
                },
                {
                    data: "qtyonhand",
                },
                {
                    data: "producttypeid",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ProductEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='tProductDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function ProductAction() {
    var action = '';
    action = document.getElementById('btnProduct').innerText;

    if (action == "Add New") {
        document.getElementById('btnProduct').innerText = 'Save';
        EnableControlPRO();
        ClearControlPRO();
        $('#productName').focus();
        //$('#dob').val(moment().format('YYYY-MM-DD'));
    }
    else if (action == "Save") {
        var data = new FormData();


        data.append("productname", $('#productName').val());
        data.append("producttypeid", $('#productTypeId').val());
        data.append("qtyonhand", $('#qtyStock').val());
        data.append("showroomid", $('#showRoomId').val());



        $.ajax({
            url: "/api/Products",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Save To Database Successfully", "Server Respond");
                $('#productTbl').DataTable().ajax.reload();
                document.getElementById('btnProduct').innerText = 'Add New';
                DisableControlPRO();
                $("#productModal").modal('hide');
            },
            error: function (errormesage) {
                $('#productname').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();


        data.append("id", $('#ID').val());

        data.append("productname", $('#productName').val());
        data.append("producttypeid", $('#productTypeId').val());
        data.append("qtyonhand", $('#qtyStock').val());
        data.append("showroomid", $('#showroomid').val());
        data.append("file_old", $("#file_old").val());

        $.ajax({
            url: "/api/Products?id=" + $("#ID").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Data has been Updated", "Server Respond");
                $('#productTbl').DataTable().ajax.reload();
                document.getElementById('btnProduct').innerText = 'Add New';
                DisableControlPRO();
                $("#productModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Data hasn't Updated in Database", "Server Respond")
            }
        });
    }
}
function ProductEdit(id) {
    //alert('hi');
    ClearControlPRO();
    EnableControlPRO();
    action = document.getElementById('btnProduct').innerText = "Update";

    $.ajax({
        url: "/api/Products?id=" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#ID').val(result.id);

            ///console.log(result.id);
            //var dr = moment(result.dob).format("YYYY-MM-DD");
            //$("#dob").val(dr);

            $("#productName").val(result.productname);
            $("#productTypeId").val(result.producttypeid);
            $("#qtyStock").val(result.qtyonhand);
            $("#showRoomid").val(result.showroomid);
            
            $("#productModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}
function ProductDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Products/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#customerTbl').DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControlPRO() {
    document.getElementById('productName').disabled = true;
    document.getElementById('productTypeId').disabled = true;
    document.getElementById('qtyStock').disabled = true;
    document.getElementById('showRoomId').disabled = true;
}

function EnableControlPRO() {
    document.getElementById('productName').disabled = false;
    document.getElementById('productTypeId').disabled = false;
    document.getElementById('qtyStock').disabled = false;
    document.getElementById('showRoomId').disabled = false;

}

function ClearControlPRO() {
    $('#productName').val('');
    $('#productTypeId').val('');
    $('#qtyStock').val('');
    $('#showRoomId').val('');
}

function AddNewProductAction() {
    document.getElementById('btnProduct').innerText = "Add New";
    DisableControlPRO();
    ClearControlPRO();
}
