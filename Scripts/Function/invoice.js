﻿$(document).ready(function () {
    GetInvoices('all');
});
$('input[name=selectdate]').change(function () {
    var selectedDate = $('#selectdate').val();
    
        GetInvoices(selectedDate);
    
});
var tableInvoices = [];
function GetInvoices(date) {
    tableInvoices = $('#invoiceTbl').dataTable({
        ajax: {
            url: "/api/Invoices1/"+ date +"/1",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "invoiceno", render: function (data) {

                        return "DX" + ("000000" + data).slice(-6);
                    }
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "customername",
                },
                {
                    data: "showroomname",
                },
                {
                    data: "totalamount",
                },
                {
                    data: "totalcarprice",
                },
                {
                    data: "totalshipprice",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='InvoiceDetail (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-th-list'></span> Detail</button>" +
                            "<button OnClick='InvoicesEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='InvoicesDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
$("#totalamount").click(function () {
    $(this).select();
});
$("#carprice").click(function () {
    $(this).select();
});
$("#shipprice").click(function () {
    $(this).select();
});
$("#alreadypaid").click(function () {
    $(this).select();
});
function getMaxInvNo() {
    $.get("/api/InvoicesNoMax", function (data, status) {
        // alert("Data: " + data + "\nStatus: " + status);
        var arrayData = data.split(',');
        //alert(arrayData[0]);
        //alert(arrayData[1]);
        $('#invoiceno').val(arrayData[1]);
        $('#invoiceNoFormat').val(arrayData[0]);

    });
}
function MaxExchange() {
    //Get Last Exchange
    $.ajax({
        url: "/api/Exchanges/1/2",
        method: "GET",
        success: function (data) {
            $("#exid").val(data.rateid);
            $("#examount").text(data.rate);
        },
        error: function (err) {
            toastr.error("no data record");
        }
    });

}
function MaxExchangebyid(id) {
    //Get Last Exchange
    $.ajax({
        url: "/api/Exchanges/" + id,
        method: "GET",
        success: function (data) {
           
            $("#examount").text(data.rate);
        },
        error: function (err) {
            toastr.error("no data record");
        }
    });

}
function InvoiceAction() {
    var chkPaid = 0;
    var action = '';
    action = document.getElementById('btnInvoice').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnInvoice').innerText = 'Save';
        EnableControlInv();
        ClearControlInv();
        $('#idate').val(moment().format('YYYY-MM-DD'));
        $('#totalamount').val('0.00');
        $('#carprice').val('0.00');
        $('#shipprice').val('0.00');
        $('#alreadypaid').val('0.00');
        getMaxInvNo();
    }

    if (action === "Save") {
        //Validate();
        if ($("#ispaid").is(':checked')) {
            chkPaid = 1;
        } else {
            chkPaid = 0;
        }
        var data = {
            date: $('#idate').val(),
            invoiceno: $('#invoiceNoFormat').val(),
            customerid: $('#customerid').val(),
            showroomid: $('#showroomid').val(),
            totalamount: $('#totalamount').val(),
            totalcarprice: $('#carprice').val(),
            totalshipprice: $('#shipprice').val(),
            alreadypaid: $('#alreadypaid').val(),
            exchangeid: $('#exid').val(),
            paid: chkPaid,

        };
        $.ajax({
            url: "/api/Invoices1",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#invoiceTbl').DataTable().ajax.reload();

                //$('#PositionModal').modal('hide');
                ClearControlInv();
                DisableControlInv();
                document.getElementById('btnInvoice').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        if ($("#ispaid").is(':checked')) {
            chkPaid = 1;
        } else {
            chkPaid = 0;
        }
        var data = {
            id: $('#ID').val(),
            date: $('#idate').val(),
            invoiceno: $('#invoiceNoFormat').val(),
            customerid: $('#customerid').val(),
            showroomid: $('#showroomid').val(),
            totalamount: $('#totalamount').val(),
            totalcarprice: $('#carprice').val(),
            totalshipprice: $('#shipprice').val(),
            alreadypaid: $('#alreadypaid').val(),
            exchangeid: $('#exid').val(),
            paid: chkPaid
        };
        $.ajax({
            url: "/api/Invoices1/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#invoiceTbl').DataTable().ajax.reload();


                ClearControlInv();
                DisableControlInv();
                document.getElementById('btnInvoice').innerText = 'Add New';

                //$('#PositionModal').modal('hide');
                //tablePositions.ajax.reload();
                //$('#registerModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}
function InvoicesEdit(id) {
    //ClearControlSh();
    $('#invoiceModal').modal('show');
    EnableControlInv();
    action = document.getElementById('btnInvoice').innerText = "Update";

    $.ajax({
        url: "/api/Invoices1/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#idate").val(dr);
            $('#invoiceno').val("DX" + ("000000" + result.invoiceno).slice(-6));
            $('#invoiceNoFormat').val(result.invoiceno);
            $('#customerid').val(result.customerid);
            $('#showroomid').val(result.showroomid);
            $('#totalamount').val(result.totalamount);
            $('#carprice').val(result.totalcarprice);
            $('#shipprice').val(result.totalshipprice);
            $('#alreadypaid').val(result.alreadypaid);
            $('#exid').val(result.exchangeid);
            $("#ispaid").prop('checked', result.paid > 0);
            MaxExchangebyid(result.exchangeid);


        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}
function InvoicesDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Invoices1/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#invoiceTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControlInv() {
    document.getElementById('idate').disabled = true;
    document.getElementById('customerid').disabled = true;
    document.getElementById('showroomid').disabled = true;
    document.getElementById('invoiceno').disabled = true;
    document.getElementById('totalamount').disabled = true;
    document.getElementById('carprice').disabled = true;
    document.getElementById('shipprice').disabled = true;
    document.getElementById('ispaid').disabled = true;
    document.getElementById('alreadypaid').disabled = true;
}
function EnableControlInv() {
    document.getElementById('idate').disabled = false;
    document.getElementById('customerid').disabled = false;
    document.getElementById('showroomid').disabled = false;
    document.getElementById('invoiceno').disabled = false;
    document.getElementById('totalamount').disabled = false;
    document.getElementById('carprice').disabled = false;
    document.getElementById('shipprice').disabled = false;
    document.getElementById('ispaid').disabled = false;
    document.getElementById('alreadypaid').disabled = false;
}
function ClearControlInv() {
    $('#idate').val('');
    $('#customerid').val('');
    $('#showroomid').val('');
    $('#invoiceno').val('');
    $('#totalamount').val('');
    $('#carprice').val('');
    $('#shipprice').val('');
    $('#ispaid').val('');
    $('#alreadypaid').val('');
}
function AddNewInvoiceAction() {
    document.getElementById('btnInvoice').innerText = "Add New";
    DisableControlInv();
    ClearControlInv();
    MaxExchange();
}

//Get Barcode By Datetime mm/dd/yyyy
function GetBarcode() {
    let currentDate = new Date();
    alert(currentDate);
}
function InvoiceDetail(id) {
    $("#invoiceDetailModal").modal('show');
    GetInvoiceDetail(id);
    $('#invoiceId').val(id);
}
var tableInvoiceDetail = [];
function GetInvoiceDetail(id) {
    tableInvoiceDetail = $('#invoiceDetailTbl').dataTable({
        ajax: {
            url: "/api/InvoiceDetail/" + id + "/1",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id", 
                },
                {
                    data: "invoicedate",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "location",
                },
                {
                    data: "productname",
                },
                {
                    data: "employeename",
                },
                {
                    data: "status",
                },
                {
                    data: "deliverytype",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='InvoiceDetailEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='InvoiceDetailDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function InvoiceDetailAction() {
    var chkPaid = 0;
    var action = '';
    action = document.getElementById('btnInvoiceDetail').innerText;
    //DisableControl();
    if (action == "Add New") {
        GetBarcode();
        document.getElementById('btnInvoiceDetail').innerText = 'Save';
        //EnableControlInv();
        //ClearControlInv();
        $('#price').val('0.00');
        $('#pricekh').val('0.00');
        $('#carprice').val('0.00');
        $('#carpricekh').val('0.00');
        $('#shipprice').val('0.00');
        $('#shippricekh').val('0.00');
        //GetBarcode();
    }

    if (action === "Save") {
        if ($("#paidtodriver").is(':checked')) {
            chkPaid = 1;
        } else {
            chkPaid = 0;
        }
        var data = {
            invoiceid: $('#invoiceId').val(),
            locationid: $('#locationid').val(),
            productid: $('#productid').val(),
            employeeid: $('#employeeid').val(),
            deliverytype: $('#deliverytype').val(),
            receiverphone: $('#receiverphone').val(),
            price: $('#price').val(),
            pricekh: $('#pricekh').val(),
            carprice: $('#carprice').val(),
            shipprice: $('#shipprice').val(),
            namepickup: $('#price').val(),
            qtybox: $('#qtybox').val(),
            barcode: $('#barcode').val(),
            status: $('#statusi').val(),
            shipperoutid: $('#shipperoutid').val(),
            carpricekh: $('#carpricekh').val(),
            shippricekh: $('#shippricekh').val(),
            paidtoshop: $('#paidtoshop').val(),
            //exchangeid: $('#exid').val(),
            paidtodriver: chkPaid,

        };
        $.ajax({
            url: "/api/InvoiceDetail",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#invoiceDetailTbl').DataTable().ajax.reload();
                //ClearControlInv();
                //DisableControlInv();
                document.getElementById('btnInvoiceDetail').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        //alert('hi');
        if ($("#ispaid").is(':checked')) {
            chkPaid = 1;
        } else {
            chkPaid = 0;
        }
        if ($("#paidtodriver").is(':checked')) {
            chkPaid = 1;
        } else {
            chkPaid = 0;
        }
        var data = {
            id: $('#ID').val(),
            invoiceid: $('#invoiceId').val(),
            locationid: $('#locationid').val(),
            productid: $('#productid').val(),
            employeeid: $('#employeeid').val(),
            deliverytype: $('#deliverytype').val(),
            receiverphone: $('#receiverphone').val(),
            price: $('#price').val(),
            pricekh: $('#pricekh').val(),
            carprice: $('#carprice').val(),
            shipprice: $('#shipprice').val(),
            namepickup: $('#price').val(),
            qtybox: $('#qtybox').val(),
            barcode: $('#barcode').val(),
            status: $('#statusi').val(),
            shipperoutid: $('#shipperoutid').val(),
            carpricekh: $('#carpricekh').val(),
            shippricekh: $('#shippricekh').val(),
            paidtoshop: $('#paidtoshop').val(),
            paidtodriver: chkPaid,

        };
        $.ajax({
            url: "/api/InvoiceDetail/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#invoiceDetailTbl').DataTable().ajax.reload();
                //ClearControlInv();
                //DisableControlInv();
                document.getElementById('btnInvoiceDetail').innerText = 'Add New';

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}
function InvoiceDetailEdit(id) {
    //ClearControlSh();
    //EnableControlInv();
    action = document.getElementById('btnInvoiceDetail').innerText = "Update";

    $.ajax({
        url: "/api/InvoiceDetail/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            $('#locationid').val(result.locationid);
            $('#productid').val(result.productid);
            $('#employeeid').val(result.employeeid);
            $('#deliverytype').val(result.deliverytype);
            $('#receiverphone').val(result.receiverphone);
            $('#price').val(result.price);
            $('#pricekh').val(result.pricekh);
            $('#carprice').val(result.carprice);
            $('#shipprice').val(result.shipprice);
            $('#status').val(result.status);
            $('#namepickup').val(result.namepickup);
            $('#qtybox').val(result.qtybox);
            $('#barcode').val(result.barcode);
            $('#statusi').val(result.status);
            $('#carpricekh').val(result.carpricekh);
            $('#shippricekh').val(result.shippricekh);
            $('#paidtoshop').val(result.paidtoshop);
            $("#paidtodriver").prop('checked', result.paid > 0);
            //MaxExchangebyid(result.exchangeid);


        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}
function InvoiceDetailDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/InvoiceDetail/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#invoiceDetailTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}














//function Validate() {
//    var isValid = true;
//    var formAddEdit = $("#formTrainingProgramAdd");
//    if ($('#proName').val().trim() === "") {
//        $('#proName').css('border-color', 'red');
//        $('#proName').focus();
//        toastr.info("Please enter Position Name", "Required");
//    }
//    else {
//        $('#proName').css('border-color', '#cccccc');
//    }
//    return isValid;
//}