﻿$(document).ready(function () {
    $('#departmentModal').on('show.bs.modal', function () {
        GetDeparment();
        DisableControlD();
    });
});

var departmentTable = [];
function GetDeparment() {
    departmentTable = $('#Departmenttbl').dataTable({
        ajax: {
            url: "/api/Departments",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "name"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CompanyTypeEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='CompanyTypeDelete (" + data + ")' class='btn btn-default btn-xs' >Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}
function DepartmentAction() {
    var action = '';
    action = document.getElementById('btnDepartment').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnDepartment').innerText = 'Save';
        EnableControlD();
        ClearControlD();
        $('#departmentName').focus();
    }
    else if (action == "Save") {
        if ($('#departmentName').val().trim() == "") {
            $('#departmentName').css('border-color', 'red');
            $('#departmentName').focus();
            toastr.info("Please Input Department Name", "Server Respon")
        }
        else {
            $('#departmentName').css('border-color', '#cccccc');
            var data = {
                name: $('#departmentName').val()
            };
            $.ajax({
                url: "/api/Departments",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Data has been Created", "Server Respond");
                    departmentTable.DataTable().ajax.reload();
                    document.getElementById('btnDepartment').innerText = 'Add New';
                    ClearControlD();
                    DisableControlD();
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database", "Server Respond")
                }
            });
        }
    } else if (action == "Update") {
        if ($('#departmentName').val().trim() == "") {
            $('#departmentName').css('border-color', 'red');
            $('#departmentName').focus();
            toastr.info("Please Input Deparment Name", "Server Respon")
        }
        else {
            $('#departmentName').css('border-color', '#cccccc');
            var data = {
                id: $('#ID').val(),
                name: $('#departmentName').val()
            };
            $.ajax({
                url: "/api/Departments/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Data has been Updated", "Server Respond");
                    departmentTable.DataTable().ajax.reload();
                    document.getElementById('btnDepartment').innerText = 'Add New';
                    ClearControlD();
                    DisableControlD();
                },
                error: function (errormesage) {
                    toastr.error("Data hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }
}

//Delete
function CompanyTypeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Departments/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        departmentTable.DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Data hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}

//Edit
function CompanyTypeEdit(id) {
    $.ajax({
        url: "/api/Departments/" + id,
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#departmentName').val(result.name);
            $('#ID').val(result.id);

            document.getElementById('btnDepartment').innerText = 'Update';
            $('#departmentName').focus();

        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}
function DisableControlD() {
    document.getElementById('departmentName').disable = true;
}
function EnableControlD() {
    document.getElementById('departmentName').disable = false;
}
function ClearControlD() {
    $('#departmentName').val('');
}
function DepartmentModalAction() {
    document.getElementById('btnDepartment').innerText = 'Add New';
    DisableControlD();
    ClearControlD();
}
