﻿$(document).ready(function () {

    $("#displaycompany").on('change', function () {
        var companyId = this.value;
        //alert(companyId);
        if (companyId == '-----Select Company-----') {
            GetVacancies('All');
            $('#btnPostNewVacancy').attr('disabled', 'disable');
        }
        else {
            GetVacancies(companyId);
            $('#btnPostNewVacancy').removeAttr('disabled');
        }
    });  
    GetVacancies('All');
    $('#btnPostNewVacancy').attr('disabled', 'disable');
});
var tableVacancy = [];
function GetVacancies(companyId) {
    tableVacancy = $('#tblVacancy').dataTable({
        ajax: {
            url: (companyId == 'All') ? "/api/Vacancies?companyId=All" : "/api/Vacancies?companyId=" + companyId ,
            dataSrc: "",
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "company.companyName",
                },
                {
                    data: "position.positionName",
                },
                {
                    data: "category.categoryName",
                },
                {
                    data: "positionAvailable",
                },
                {
                    data: "contractType",
                },
                {
                    data: "shcedule",
                },
                {
                    data: "salary",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='VacancyEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='VacancyDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}
function VacanyAction() {
    var action = '';
    action = document.getElementById('btnSave').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnSave').innerText = 'Save';
        EnableControl();
        $('#position').focus();
    }

    if (action === "Save") {
        //Validate();
        var data = {
            positionId: $('#position').val(),
            categoryId: $('#category').val(),
            positionAvailable: $('#positionAvailable').val(),
            salary: $('#salary').val(),
            shcedule: $('#schedule').val(),
            companyId: $('#displaycompany').val(),
            location: $('#location').val(),
            contractType: $('#contractType').val(),
            responsiblities: $('#responsibility').val(),
            requirement: $('#requirement').val(),
            applicationInformation: $('#information').val(),
            postingDate: $('#postingDate').val(),
            deadLine: $('#deadline').val(),
            status: $('#sstatus').val(),

        };
        $.ajax({
            url: "/api/Vacancies",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#tblVacancy').DataTable().ajax.reload();

                $('#vacancyModal').modal('hide');
                ClearControl();
                DisableControl();
                document.getElementById('btnSave').innerText = 'Add New';

                //document.getElementById('btnSavePosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            },
            error: function (errormessage) {
                toastr.error("This Position is already exists.", "Server Response");
                //document.getElementById('btnSavePosition').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

            }
        });
    }
}

function Update() {
    //alert('hi');
    var data = {
        id: $('#id').val(),
        positionId: $('#position').val(),
        categoryId: $('#category').val(),
        positionAvailable: $('#positionAvailable').val(),
        salary: $('#salary').val(),
        shcedule: $('#schedule').val(),
        companyId: $('#displaycompany').val(),
        location: $('#location').val(),
        contractType: $('#contractType').val(),
        responsiblities: $('#responsibility').val(),
        requirement: $('#requirement').val(),
        applicationInformation: $('#information').val(),
        postingDate: $('#postingDate').val(),
        deadLine: $('#deadline').val(),
        status: $('#sstatus').val(),
    };
    $.ajax({
        url: "/api/Vacancies/" + data.id,
        data: JSON.stringify(data),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Updated successfully.", "Server Response");
            $('#tblVacancy').DataTable().ajax.reload();

            ClearControl();
            DisableControl();
            $('#vacancyModal').modal('hide');
            //document.getElementById('btnSavePosition').innerText = 'Add New';

            //$('#PositionModal').modal('hide');
            //tablePositions.ajax.reload();
            //$('#registerModal').modal('hide');

        },
        error: function (errormessage) {
            toastr.error("This Position can't Update.", "Server Response");

        }
    });
}

function VacancyDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Vacancies/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblVacancy').DataTable().ajax.reload();

                        toastr.success("Vancancy Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Vancancy Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function VacancyEdit(id) {
    ClearControl();
    EnableControl();
    //$('#status').val('');
    action = document.getElementById('btnUpdate').innerText = "Update";

    $.ajax({
        url: "/api/Vacancies/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#id').val(result.id);
            $('#position').val(result.positionId);
            $('#category').val(result.categoryId);
            $('#positionAvailable').val(result.positionAvailable);
            $('#salary').val(result.salary);
            $('#schedule').val(result.shcedule);
            $('#displaycompany').val(result.companyId);
            $('#location').val(result.location);
            $('#contractType').val(result.contractType);
            $('#responsibility').val(result.responsiblities);
            $('#requirement').val(result.requirement);
            $('#information').val(result.applicationInformation);
            $('#postingDate').val(result.postingDate);
            $('#deadline').val(result.deadLine);
            $('#sstatus').val(result.status);

            $('#vacancyModal').modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function DisableControl() {
    document.getElementById('position').disabled = true;
    document.getElementById('category').disabled = true;
    document.getElementById('positionAvailable').disabled = true;
    document.getElementById('contractType').disabled = true;
    document.getElementById('salary').disabled = true;
    document.getElementById('schedule').disabled = true;
    document.getElementById('location').disabled = true;
    document.getElementById('responsibility').disabled = true;
    document.getElementById('requirement').disabled = true;
    document.getElementById('information').disabled = true;
    document.getElementById('postingDate').disabled = true;
    document.getElementById('deadline').disabled = true;
    document.getElementById('sstatus').disabled = true;

}

function EnableControl() {

    document.getElementById('position').disabled = false;
    document.getElementById('category').disabled = false;
    document.getElementById('positionAvailable').disabled = false;
    document.getElementById('contractType').disabled = false;
    document.getElementById('salary').disabled = false;
    document.getElementById('schedule').disabled = false;
    document.getElementById('location').disabled = false;
    document.getElementById('responsibility').disabled = false;
    document.getElementById('requirement').disabled = false;
    document.getElementById('information').disabled = false;
    document.getElementById('postingDate').disabled = false;
    document.getElementById('deadline').disabled = false;
    document.getElementById('sstatus').disabled = false;
}

function ClearControl() {
    $('#position').val('');
    $('#category').val('');
    $('#positionAvailable').val('');
    $('#contractType').val('');
    $('#salary').val('');
    $('#schedule').val('');
    $('#location').val('');
    $('#responsibility').val('');
    $('#requirement').val('');
    $('#information').val('');
    $('#postingDate').val('');
    $('#deadline').val('');
    $('#sstatus').val('');

}

function AddnewVacancyAction() {
    document.getElementById('btnSave').innerText = "Add New";
    DisableControl();
    ClearControl();
}