﻿$(document).ready(function () {
    GetEmployee('All');
    $('#displayDepartment').on('change', function () {
        var departmentId = this.value;
        if (departmentId == '-----Select Department-----') {
            GetEmployee('All');
        } else {
            GetEmployee(departmentId);
        }
    })
    $('#salaryModal').on('show.bs.modal', function () {
        
    });
   
});
var tableEmployee = [];
var tableSalary = [];
var tableBonus = [];
function GetEmployee(departmentid) {
    tableEmployee = $('#employeeTbl').DataTable({
        ajax: {
            url: (departmentid == 'all') ? "/api/Employees?departmentid=All" : "/api/Employees?departmentid=" + departmentid,
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "name",
                },
                {
                    data: "namekh",
                },
                {
                    data: "sex",
                },
                {
                    data: "phone",
                },
                {
                    data: "address",
                },
                {
                    data: "phonecard",
                },
                {
                    data: "petoluem",
                },
                {
                    data: "deliveryin",
                },
                {
                    data: "deliveryout",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='EmployeeEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='EmployeeDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>" +
                            "<button OnClick='Salary (" + data + ")' class='btn btn-primary btn-xs' ><span class='glyphicon glyphicon-usd'></span> Salary</button>" +
                            "<button OnClick='Bonus (" + data + ")' class='btn btn-success btn-xs' ><span class='glyphicon glyphicon-usd'></span> Bonus</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function readURLEmployee(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#employeePhoto").attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
};
function EmployeeAction() {
    var action = '';
    action = document.getElementById('btnEmp').innerText;

    if (action == "Add New") {
        document.getElementById('btnEmp').innerText = 'Save';
        EnableControlEm();
        ClearControlEm();
        $('#empname').focus();
        $('#dob').val(moment().format('YYYY-MM-DD'));
        $('#employeePhoto').attr('src', '../Images/company.png');
        $('#phone_card').val('0');
        $('#petroluem').val('0');
        $('#deliveryin').val('0');
        $('#deliveryout').val('0');
    }
    else if (action == "Save") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("photo", files[0]);
        }

        data.append("name", $('#empname').val());
        data.append("namekh", $('#empnamekh').val());
        data.append("sex", $('#sex').val());
        data.append("phone", $('#phone').val());
        data.append("dob", $('#dob').val());
        data.append("address", $('#address').val());
        data.append("departmentid", $('#departmentid').val());
        data.append("positionid", $('#positionid').val());
        data.append("showroomid", $('#showroomid').val());
        data.append("identityno", $('#identityno').val());
        data.append("shippertype", $('#shippertype').val());
        data.append("vehicle", $('#vehiracle').val());
        data.append("plateno", $('#plateno').val());
        data.append("phonecard", $('#phone_card').val());
        data.append("petoluem", $('#petroluem').val());
        data.append("deliveryin", $('#deliveryin').val());
        data.append("deliveryout", $('#deliveryout').val());
        data.append("email", $('#email').val());



        $.ajax({
            url: "/api/Employees",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Save To Database Successfully", "Server Respond");
                $('#employeeTbl').DataTable().ajax.reload();
                document.getElementById('btnEmp').innerText = 'Add New';
                DisableControlEm();
                $("#employeeModal").modal('hide');
            },
            error: function (errormesage) {
                $('#empname').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("photo", files[0]);
        }

        data.append("id", $('#id').val());

        data.append("name", $('#empname').val());
        data.append("namekh", $('#empnamekh').val());
        data.append("sex", $('#sex').val());
        data.append("phone", $('#phone').val());
        data.append("dob", $('#dob').val());
        data.append("address", $('#address').val());
        data.append("departmentid", $('#departmentid').val());
        data.append("positionid", $('#positionid').val());
        data.append("showroomid", $('#showroomid').val());
        data.append("identityno", $('#identityno').val());
        data.append("shippertype", $('#shippertype').val());
        data.append("vehicle", $('#vehiracle').val());
        data.append("plateno", $('#plateno').val());
        data.append("phonecard", $('#phone_card').val());
        data.append("petoluem", $('#petroluem').val());
        data.append("deliveryin", $('#deliveryin').val());
        data.append("deliveryout", $('#deliveryout').val());
        data.append("email", $('#email').val());
        data.append("file_old", $("#file_old").val());

        $.ajax({
            url: "/api/Employees?id=" + $("#id").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Data has been Updated", "Server Respond");
                $('#employeeTbl').DataTable().ajax.reload();
                document.getElementById('btnEmp').innerText = 'Add New';
                DisableControlEm();
                $("#employeeModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Data hasn't Updated in Database", "Server Respond")
            }
        });
    }
}
function EmployeeEdit(id) {
    //alert('hi');
    ClearControlEm();
    EnableControlEm();
    action = document.getElementById('btnEmp').innerText = "Update";

    $.ajax({
        url: "/api/Employees?id=" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#id').val(result.id);

            ///console.log(result.id);
            var dr = moment(result.dob).format("YYYY-MM-DD");
            $("#dob").val(dr);

            $("#empname").val(result.name);
            $("#empnamekh").val(result.namekh);
            $("#sex").val(result.sex);
            $("#departmentid").val(result.departmentid);
            $("#email").val(result.email);
            $("#address").val(result.address);
            $("#positionid").val(result.positionid);
            $("#showroomid").val(result.showroomid);
            $("#phone").val(result.phone);
            $("#identityno").val(result.identityno);
            $("#shippertype").val(result.shippertype);
            $("#vehiracle").val(result.vehicle);
            $("#plateno").val(result.plateno);
            $("#phone_card").val(result.phonecard);
            $("#petroluem").val(result.petoluem);
            $("#deliveryin").val(result.deliveryin);
            $("#deliveryout").val(result.deliveryout);
            $('#file_old').val(result.photo);

            if (result.photo == "") {
                $("#employeePhoto").attr('src', '../Images/company.png');
            } else {
                $("#employeePhoto").attr('src', '../Images/' + result.photo);
            }

            $("#employeeModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}
function EmployeeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Employees/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#employeeTbl').DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControlEm() {
    document.getElementById('empname').disabled = true;
    document.getElementById('empnamekh').disabled = true;
    document.getElementById('sex').disabled = true;
    document.getElementById('departmentid').disabled = true;
    document.getElementById('email').disabled = true;
    document.getElementById('address').disabled = true;
    document.getElementById('positionid').disabled = true;
    document.getElementById('dob').disabled = true;
    document.getElementById('phone').disabled = true;
    document.getElementById('identityno').disabled = true;
    document.getElementById('shippertype').disabled = true;
    document.getElementById('vehiracle').disabled = true;
    document.getElementById('plateno').disabled = true;
    document.getElementById('phone_card').disabled = true;
    document.getElementById('petroluem').disabled = true;
    document.getElementById('deliveryin').disabled = true;
    document.getElementById('deliveryout').disabled = true;
}

function EnableControlEm() {
    document.getElementById('empname').disabled = false;
    document.getElementById('empnamekh').disabled = false;
    document.getElementById('sex').disabled = false;
    document.getElementById('departmentid').disabled = false;
    document.getElementById('email').disabled = false;
    document.getElementById('address').disabled = false;
    document.getElementById('positionid').disabled = false;
    document.getElementById('dob').disabled = false;
    document.getElementById('phone').disabled = false;
    document.getElementById('identityno').disabled = false;
    document.getElementById('shippertype').disabled = false;
    document.getElementById('vehiracle').disabled = false;
    document.getElementById('plateno').disabled = false;
    document.getElementById('phone_card').disabled = false;
    document.getElementById('petroluem').disabled = false;
    document.getElementById('deliveryin').disabled = false;
    document.getElementById('deliveryout').disabled = false;

}

function ClearControlEm() {
    $('#empname').val('');
    $('#empnamekh').val('');
    $('#sex').val('');
    $('#departmentid').val('');
    $('#email').val('');
    $('#address').val('');
    $('#positionid').val('');
    $('#dob').val('');
    $('#phone').val('');
    $('#identityno').val('');
    $('#shippertype').val('');
    $('#vehiracle').val('');
    $('#plateno').val('');
    $('#phone_card').val('');
    $('#petroluem').val('');
    $('#deliveryin').val('');
    $('#deliveryout').val('');

}

function EmployeeModalAction() {
    document.getElementById('btnEmp').innerText = "Add New";
    DisableControlEm();
    ClearControlEm();
}

function Salary(id) {
    $("#salaryModal").modal('show');
    $('#empid').val(id);
    GetSalary(id);
    ClearControlSA();
    DisableControlSA();
}
function GetSalary(id) {
    tableSalary = $('#SalaryTbl').DataTable({
        ajax: {
            url: "/api/SalaryByEmp/" + id + "/2",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "amount",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='SalaryEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='SalaryDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function SalaryAction() {
    var action = '';
    action = document.getElementById('btnSalary').innerText;

    if (action == "Add New") {
        document.getElementById('btnSalary').innerText = 'Save';
        EnableControlSA();
        ClearControlSA();
        $('#date').val(moment().format('YYYY-MM-DD'));
        $('#amount').focus();
        $('#amount').val('0');
    } if (action === "Save") {
        var data = {
            date: $('#date').val(),
            amount: $('#amount').val(),
            employeeid: $('#empid').val(),
        };
        $.ajax({
            url: "/api/Salary",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#SalaryTbl').DataTable().ajax.reload();
                ClearControlSA();
                DisableControlSA();
                document.getElementById('btnSalary').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        var data = {
            id: $('#sid').val(),
            date: $('#date').val(),
            amount: $('#amount').val(),
            employeeid: $('#empid').val(),
        };
        $.ajax({
            url: "/api/Salary/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#SalaryTbl').DataTable().ajax.reload();
                ClearControlSA();
                DisableControlSA();
                document.getElementById('btnSalary').innerText = 'Add New';

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}
function SalaryEdit(id) {
    ClearControlSA();
    EnableControlSA();
    action = document.getElementById('btnSalary').innerText = "Update";

    $.ajax({
        url: "/api/Salary/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#sid').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $('#date').val(dr);
            $('#amount').val(result.amount);
            $('#empid').val(result.employeeid);
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}
function SalaryDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Salary/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#SalaryTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControlSA() {
    document.getElementById('date').disabled = true;
    document.getElementById('amount').disabled = true;
}
function EnableControlSA() {
    document.getElementById('date').disabled = false;
    document.getElementById('amount').disabled = false;
}
function ClearControlSA() {
    $('#date').val('');
    $('#amount').val('');
}
function Bonus(id) {
    $("#bonusModal").modal('show');
    $('#empidB').val(id);
    GetBonus(id);
    ClearControlBO();
    DisableControlBO();
}
function GetBonus(id) {
    tableBonus = $('#BonusTbl').DataTable({
        ajax: {
            url: "/api/BonusesByEmp/" + id + "/2",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "type",
                },
                {
                    data: "amount",
                },
                {
                    data: "amountriel",
                },
                {
                    data: "note",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='BonusEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='BonusDelete (" + data + ")' class='btn btn-default btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function BonusAction() {
    var action = '';
    action = document.getElementById('btnBonus').innerText;

    if (action == "Add New") {
        document.getElementById('btnBonus').innerText = 'Save';
        EnableControlBO();
        ClearControlBO();
        $('#dateB').val(moment().format('YYYY-MM-DD'));
        $('#amountB').focus();
        $('#amountB').val('0');
        $('#amountRiel').val('0');
    } if (action === "Save") {
        var data = {
            date: $('#dateB').val(),
            type: $('#type').val(),
            amountriel: $('#amountRiel').val(),
            amount: $('#amountB').val(),
            note: $('#note').val(),
            employeeid: $('#empidB').val(),
        };
        $.ajax({
            url: "/api/Bonuses",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#BonusTbl').DataTable().ajax.reload();
                ClearControlBO();
                DisableControlBO();
                document.getElementById('btnBonus').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        var data = {
            id: $('#bid').val(),
            date: $('#dateB').val(),
            type: $('#type').val(),
            amountriel: $('#amountRiel').val(),
            amount: $('#amountB').val(),
            note: $('#note').val(),
            employeeid: $('#empidB').val(),
        };
        $.ajax({
            url: "/api/Bonuses/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#BonusTbl').DataTable().ajax.reload();
                ClearControlBO();
                DisableControlBO();
                document.getElementById('btnBonus').innerText = 'Add New';

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}
function BonusEdit(id) {
    ClearControlBO();
    EnableControlBO();
    action = document.getElementById('btnBonus').innerText = "Update";

    $.ajax({
        url: "/api/Bonuses/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#bid').val(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $('#dateB').val(dr);
            $('#type').val(result.type);
            $('#amount').val(result.amount);
            $('#amountRiel').val(result.amountriel);
            $('#note').val(result.note);
            $('#empidB').val(result.employeeid);
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}
function BonusDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Bonuses/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#BonusTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}
function DisableControlBO() {
    document.getElementById('dateB').disabled = true;
    document.getElementById('amountB').disabled = true;
    document.getElementById('amountRiel').disabled = true;
    document.getElementById('note').disabled = true;
    document.getElementById('type').disabled = true;
}
function EnableControlBO() {
    document.getElementById('dateB').disabled = false;
    document.getElementById('amountB').disabled = false;
    document.getElementById('amountRiel').disabled = false;
    document.getElementById('note').disabled = false;
    document.getElementById('type').disabled = false;
}
function ClearControlBO() {
    $('#dateB').val('');
    $('#amountB').val('');
    $('#amountRiel').val('');
    $('#type').val('');
    $('#note').val('');
}