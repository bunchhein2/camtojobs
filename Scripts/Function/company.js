﻿
$(document).ready(function () {

    GetCompany();

    $('#companyModal').on('show.bs.modal', function () {        
        //document.getElementById('companyTypeName').disable = true;
    });    
});
var tableCompany = [];
function GetCompany() {
    tableCompany = $('#tblComanies').dataTable({
        ajax: {
            url: "/api/Companies",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "companyName"
                },
                {
                    data: "companyType.name"
                },
                {
                    data: "contactPerson"
                },
                {
                    data: "contactPhone"
                },
                {
                    data: "email"
                },
                {
                    data: "address"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CompanyEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='CompanyDelete (" + data + ")' class='btn btn-default btn-xs' >Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}
//import { filereader } from "modernizr";

function readURLCompany(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#companyPhoto").attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
};


//Save  
function CompanyAction() {
    var action = '';
    action = document.getElementById('saveCompany').innerText;

    if (action == "Add New") {
        document.getElementById('saveCompany').innerText = 'Save';
        EnableControl();
        $('#companyName').focus();
    }
    else if (action == "Save") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("Photo", files[0]);
        }

        data.append("CompanyName", $('#companyName').val());
        data.append("CompanyTypeId", $('#companyTypeId').val());
        data.append("ContactPerson", $('#contactPerson').val());
        data.append("ContactPhone", $('#contactPhone').val());
        data.append("Email", $('#email').val());
        data.append("Address", $('#address').val());


        $.ajax({
            url: "/api/Companies",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("New Company has been Created", "Server Respond");
                $('#tblComanies').DataTable().ajax.reload();
                document.getElementById('saveCompany').innerText = 'Add New';
                // $('#customerName').val('');
                $("#companyModal").modal('hide');
            },
            error: function (errormesage) {
                $('#companyName').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        //alert('hi');
        //var res = ValidateCustomer();
        //if (res == false) {
        //    return false;
        //}
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("Photo", files[0]);
        }

        data.append("Id", $('#companyID').val());

        data.append("CompanyName", $('#companyName').val());
        data.append("CompanyTypeId", $('#companyTypeId').val());
        data.append("ContactPerson", $('#contactPerson').val());
        data.append("ContactPhone", $('#contactPhone').val());
        data.append("Email", $('#email').val());
        data.append("Address", $('#address').val());

        $.ajax({
            url: "/api/Companies/" + $("#companyID").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Company has been Updated", "Server Respond");
                $('#tblComanies').DataTable().ajax.reload();
                $("#companyModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Company hasn't Updated in Database", "Server Respond")
            }
        });
    }
}

function CompanyEdit(id) {
    //alert('hi');
    //ClearControl();
    //EnableControl();
    action = document.getElementById('saveCompany').innerText = "Update";

    $.ajax({
        url: "/api/Companies/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#companyID').val(result.id);

            ///console.log(result.id);
            //var date = new Date(result.date);
            //var datetime = moment(date).format('YYYY-MM-DD');
            //$("#date").val(datetime);
            $("#companyName").val(result.companyName);
            $("#companyTypeId").val(result.companyTypeId);
            $("#contactPerson").val(result.contactPerson);
            $("#contactPhone").val(result.contactPhone);
            $("#email").val(result.email);
            $("#address").val(result.address);

            if (result.photo == "") {
                $("#companyPhoto").attr('src', '../Images/company.png');
            } else {
                $("#companyPhoto").attr('src', '../Images/' + result.photo);
            }

            $("#companyModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}

function CompanyDelete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Companies/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblComanies').DataTable().ajax.reload();
                        toastr.success("Company Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Company Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}


function DisableControl() {
    document.getElementById('companyName').disabled = true;
    document.getElementById('companyTypeId').disabled = true;
    document.getElementById('contactPerson').disabled = true;
    document.getElementById('contactPhone').disabled = true;
    document.getElementById('email').disabled = true;
    document.getElementById('address').disabled = true;


}

function EnableControl() {
    document.getElementById('companyName').disabled = false;
    document.getElementById('companyTypeId').disabled = false;
    document.getElementById('contactPerson').disabled = false;
    document.getElementById('contactPhone').disabled = false;
    document.getElementById('email').disabled = false;
    document.getElementById('address').disabled = false;

}

function ClearControl() {
    $('#companyName').val('');
    $('#companyTypeId').val('');
    $('#contactPerson').val('');
    $('#contactPhone').val('');
    $('#email').val('');
    $('#address').val('');

}

function AddnewAction() {
    document.getElementById('saveCompany').innerText = "Add New";
    DisableControl();
    ClearControl();
}

/** function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#deliveryName').val().trim() === "") {
        $('#deliveryName').css('border-color', 'red');
        $('#deliveryName').focus();
        toastr.info("Please enter date", "Required");
    }
    else {
        $('#deliveryName').css('border-color', '#cccccc');
        //        if ($('#phone').val().trim() === "") {
        //            $('#phone').css('border-color', 'red');
        //            $('#phone').focus();
        //            toastr.info("Please enter your phone", "Required");
        //        }
        //        else {
        //            $('#phone').css('border-color', '#cccccc');
        //        }
        //    }
        //    return isValid;
        //}

        //  function ValidateCustomer() {
        //    var isValid = true;
        //    var formAddEdit = $("#formTrainingProgramAdd");
        //      if ($('#date').val().trim() == "") {
        //          $(' #date').css('border-color', 'red');
        //          $(' #date').focus();
        //        isValid = false;
        //    } else {
        //          $(' #date').css('border-color', '#cccccc');
        //          $(' #date').focus();
        //    }
        //      if ($(' #phone').val().trim() == "") {
        //          $(' #phone').css('border-color', 'red');
        //        isValid = false;
        //    } else {
        //          $(' #phone').css('border-color', '#cccccc');

        //    }
        //    if ($(' #currentlocation').val().trim() == "") {
        //        $(' #currentlocation').css('border-color', 'red');
        //        isValid = false;
        //    } else {
        //        $(' #currentlocation').css('border-color', '#cccccc');
        //    }
        //    if ($(' #customerName').val().trim() == "") {
        //        $(' #customerName').css('border-color', 'red');
        //        isValid = false;
        //    } else {
        //        $(' #customerName').css('border-color', '#cccccc');
        //    }
        //if ($(' #phone').val().trim() == "") {
        //    $(' #phone').css('border-color', 'red');
        //    isValid = false;
        //} else {
        //    $(' #phone').css('border-color', '#cccccc');
    }

    return isValid;
}
*/
