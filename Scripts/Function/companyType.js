﻿$(document).ready(function () {
    $('#companyTypeModal').on('show.bs.modal', function () {
        GetCompanyType();
        document.getElementById('companyTypeName').disable = true;
    });    
});
var tableCompanyType = [];
function GetCompanyType() {
    tableCompanyType = $('#Companytbl').dataTable({
        ajax: {
            url: "/api/companyTypes",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "name"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CompanyTypeEdit (" + data + ")' class='btn btn-info btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='CompanyTypeDelete (" + data + ")' class='btn btn-default btn-xs' >Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}

//Save
function CompanyTypeAction() {    
    var action = '';
    action = document.getElementById('btnCompanyType').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnCompanyType').innerText = 'Save';
        $('#companyTypeName').focus();
    }
    else if (action == "Save") {
        if ($('#companyTypeName').val().trim() == "") {
            $('#companyTypeName').css('border-color', 'red');
            $('#companyTypeName').focus();
            toastr.info("Please input companytype name", "Server Respon")
        } 
        else {
            $('#companyTypeName').css('border-color', '#cccccc');
            var data = {
                Name: $('#companyTypeName').val()
            };
            $.ajax({
                url: "/api/companyTypes",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Company Type has been Created", "Server Respond");
                    $('#Companytbl').DataTable().ajax.reload();
                    document.getElementById('btnCompanyType').innerText = 'Add New';
                    $('#companyTypeName').val('');
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database","Server Respond")
                }
            });
        }
    } else if (action == "Update") {
        if ($('#companyTypeName').val().trim() == "") {
            $('#companyTypeName').css('border-color', 'red');
            $('#companyTypeName').focus();
            toastr.info("Please input companytype name", "Server Respon")
        }
        else {
            $('#companyTypeName').css('border-color', '#cccccc');
            var data = {
                Id: $('#ID').val(),
                Name: $('#companyTypeName').val()
            };
            $.ajax({
                url: "/api/companyTypes/" + data.Id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Company Type has been Updated", "Server Respond");
                    $('#Companytbl').DataTable().ajax.reload();
                    document.getElementById('btnCompanyType').innerText = 'Add New';
                    $('#companyTypeName').val('');
                },
                error: function (errormesage) {
                    toastr.error("Company Type hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }
}

//Delete
function CompanyTypeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/companyTypes/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#Companytbl').DataTable().ajax.reload();
                        toastr.success("CompanyType Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This CompanyType hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}

//Edit
function CompanyTypeEdit(id) {
    $.ajax({
        url: "/api/companyTypes/" + id,        
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#companyTypeName').val(result.name);     
            $('#ID').val(result.id);
            
            document.getElementById('btnCompanyType').innerText = 'Update';
            $('#companyTypeName').focus();
            
        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}

