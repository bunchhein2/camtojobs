﻿using System.Web;
using System.Web.Mvc;
using CamTojobs.App_Start;

namespace CamTojobs
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new AutorizeAttribute());
            filters.Add(new HandleErrorAttribute());
            
        }
    }
}
