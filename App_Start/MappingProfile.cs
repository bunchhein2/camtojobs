﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;

namespace CamTojobs.App_Start
{
    public class MappingProfile: Profile
    {
        //private object opt;

        public MappingProfile()
        {
            //Domain To Dtos
            Mapper.CreateMap<Location, LocationDto>();
            Mapper.CreateMap<InvoiceDetail, InvoiceDetailDto>();
            Mapper.CreateMap<Invoices1, Invoices1Dto>();
            Mapper.CreateMap<Invoice, InvoiceDto>();
            Mapper.CreateMap<Exchange, ExchangeDto>();
            Mapper.CreateMap<Product, ProductDto>();
            Mapper.CreateMap<ProductType, ProductTypeDto>();
            Mapper.CreateMap<Customer, CustomerDto>();
            Mapper.CreateMap<Bonus, BonusDto>();
            Mapper.CreateMap<Salary, SalaryDto>();
            Mapper.CreateMap<Employee, EmplyeeDto>();
            Mapper.CreateMap<ShowRoom, ShowRoomDto>();
            Mapper.CreateMap<Department, DepartmentDto>();
            Mapper.CreateMap<Event, EventDto>();
            Mapper.CreateMap<Vacancy, VacancyDto>();
            Mapper.CreateMap<Category, CategoryDto>();
            Mapper.CreateMap<Position, PositionDto>();
            Mapper.CreateMap<CompanyType, CompanyTypeDtos>();
            Mapper.CreateMap<Company, CompanyDtos>();


            //Dtos To Domain
            Mapper.CreateMap<LocationDto, Location>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<InvoiceDetailDto, InvoiceDetail>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<Invoices1Dto, Invoices1>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<InvoiceDto, Invoice>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<ExchangeDto, Exchange>().ForMember(c => c.rateid, opt => opt.Ignore());
            Mapper.CreateMap<ProductDto, Product>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<ProductTypeDto, ProductType>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<CustomerDto, Customer>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<BonusDto, Bonus>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<SalaryDto, Salary>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<EmplyeeDto, Employee>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<ShowRoomDto, ShowRoom>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<CompanyTypeDtos, CompanyType>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<CompanyDtos, Company>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<CategoryDto, Category>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<PositionDto, Position>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<VacancyDto, Vacancy>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<EventDto, Event>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<DepartmentDto, Department>().ForMember(c => c.id, opt => opt.Ignore());


        }
    }
}