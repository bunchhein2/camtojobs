﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CamTojobs.Startup))]
namespace CamTojobs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
