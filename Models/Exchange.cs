﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("ExchangeRates")]
    public class Exchange
    {
        [Key]
        public int rateid { get; set; }
        public DateTime? date {get;set;}
        public decimal rate { get; set; }
        public bool status { get; set; }
        public bool IsDeleted { get; set; }
        public Exchange()
        {
            IsDeleted = false;
        }
    }
}