﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("producttype_tbl")]
    public class ProductType
    {
        public int id { get; set; }
        public string producttypename { get; set; }
        public bool status { get; set; }
    }
}