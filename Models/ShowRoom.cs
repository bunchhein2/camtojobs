﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("showroom_tbl")]
    public class ShowRoom
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string owner { get; set; }
        public string phone { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public bool status { get; set; }
    }
}