﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("salary_tbl")]
    public class Salary
    {
        public int id { get; set; }
        public DateTime? date {get;set;}
        public decimal amount { get; set; }
        public int employeeid { get; set; }
        public Employee employee { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}