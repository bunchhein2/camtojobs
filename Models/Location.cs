﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("location_tbl")]
    public class Location
    {
        public int id { get; set; }
        public string location { get; set; }
        public decimal price { get; set; }
        public bool status { get; set; }
    }
}