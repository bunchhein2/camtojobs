﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("product_tbl")]
    public class Product
    {
        public int id { get; set; }
        public string productname { get; set; }
        public int producttypeid { get; set; }
        public ProductType producttype { get; set; }
        public decimal qtyonhand { get; set; }
        public string photo { get; set; }
        public bool status { get; set; }
        public int showroomid { get; set; }
        public ShowRoom showroom { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}