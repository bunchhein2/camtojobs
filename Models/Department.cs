﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("department_tbl")]
    public class Department
    {
        public int id { get; set; }
        public string name { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}