﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    public class Category
    {
        public int id { get; set; }
        [Required]
        [stringlength(100)]
        public string categoryName { get; set; }
    }
}