﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("invoice_tbl")]
    public class Invoices1
    {
        [Key]
        public int id { get; set; }
        public DateTime? date { get; set; }
        public int invoiceno { get; set; }
        public int customerid { get; set; }
        public int showroomid { get; set; }
        public int exchangeid { get; set; }
        public decimal totalamount { get; set; }
        public decimal totalcarprice { get; set; }
        public decimal totalshipprice { get; set; }
        public decimal alreadypaid { get; set; }
        public bool status { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public bool paid { get; set; }
    }
}