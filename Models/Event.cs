﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("event_tbl")]
    public class Event
    {
        public int id { get; set; }
        [Required]
        [stringlength(100)]
        public string event_name { get; set; }
        public DateTime event_date { get; set; }
        [Required]
        [stringlength(100)]
        public string place { get; set; }
        public bool status { get; set; }
    }
}