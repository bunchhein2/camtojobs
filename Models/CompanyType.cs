﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("CompanyTypes")]
    public class CompanyType
    {
        public int Id { get; set; }

        [require]
        [stringlength(50)]
        public string Name { get; set; }
    }
}