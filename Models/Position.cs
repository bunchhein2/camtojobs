﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("Positions")]
    public class Position
    {
        public int id { get; set; }
        [Required]
        [stringlength(100)]
        public string positionName { get; set; }
    }
}