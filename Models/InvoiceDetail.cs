﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CamTojobs.Models
{
    [Table("invoicedetail_tbl")]
    public class InvoiceDetail
    {
        public int id { get; set; }
        public int invoiceid { get; set; }
        public DateTime? invoicedate { get; set; }
        public int locationid { get; set; }
        public int productid { get; set; }
        public int employeeid { get; set; }
        public string deliverytype { get; set; }
        public string receiverphone { get; set; }
        public bool paidtype { get; set; }
        public decimal price { get; set; }
        public decimal pricekh { get; set; }
        public decimal carprice { get; set; }
        public decimal shipprice { get; set; }
        public string status { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public string updateby { get; set; }
        public DateTime? updatedate { get; set; }
        public bool alreadymove { get; set; }
        public DateTime? datepickup { get; set; }
        public string namepickup { get; set; }
        public bool paidtodriver { get; set; }
        public int qtybox { get; set; }
        public string barcode { get; set; }
        public int? shipperoutid { get; set; }
        public DateTime? shipperdate { get; set; }
        public decimal carpricekh { get; set; }
        public decimal shippricekh { get; set; }
        public string paidtoshop { get; set; }
    }
}