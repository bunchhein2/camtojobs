﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CamTojobs.Models;

namespace CamTojobs.ViewModel
{
    public class VancancyViewModel
    {
        public IEnumerable<Company> Companies { get; set; }
        public IEnumerable<Position> Positions { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}