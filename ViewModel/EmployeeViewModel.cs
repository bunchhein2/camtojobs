﻿using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.ViewModel
{
    public class EmployeeViewModel
    {
        public IEnumerable<Department> Departments { get; set; }
        public IEnumerable<ShowRoom> ShowRooms { get; set; }
        public IEnumerable<Position> Positions { get; set; }

        public IEnumerable<Customer> Customers { get; set; }
        public IEnumerable<Location> Locations { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<Employee> Employees { get; set; }

    }
}