﻿using CamTojobs.Models;
using CamTojobs.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CamTojobs.Controllers
{
    public class CustomerController : Controller
    {
        ApplicationDbContext _context = new ApplicationDbContext();
        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Customers
        public ActionResult Index()
        {
            var customerVM = new CustomerViewModel()
            {
                ShowRoomss = _context.ShowRooms.ToList()
            };
            return View(customerVM);
        }
    }
}