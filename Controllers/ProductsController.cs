﻿using CamTojobs.Models;
using CamTojobs.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CamTojobs.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext _context;

        public ProductsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Products
        public ActionResult Index()
        {
            var viewModel = new CustomerViewModel()
            {
                ShowRoomss=_context.ShowRooms.ToList(),
                ProductTypes = _context.ProductTypes.ToList()
            };
            return View(viewModel);
        }
    }
}