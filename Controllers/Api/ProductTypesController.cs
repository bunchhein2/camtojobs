﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class ProductTypesController : ApiController
    {
        private ApplicationDbContext _context;

        public ProductTypesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        //Get : api/ProductTypess
        public IHttpActionResult GetProductTypes()
        {
            var getProductTypes = _context.ProductTypes.ToList().Select(Mapper.Map<ProductType, ProductTypeDto>);

            return Ok(getProductTypes);
        }


        [HttpGet]
        //Get : api/ProductTypess{id}
        public IHttpActionResult GetProductTypes(int id)
        {
            var getProductTypesById = _context.ProductTypes.SingleOrDefault(c => c.id == id);

            if (getProductTypesById == null)
                return NotFound();

            return Ok(Mapper.Map<ProductType, ProductTypeDto>(getProductTypesById));
        }
        [HttpPost]
        public IHttpActionResult CreateProductTypes(ProductTypeDto ProductTypeDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.ProductTypes.SingleOrDefault(c => c.producttypename == ProductTypeDtos.producttypename);
            if (isExist != null)
                return BadRequest();

            var ProductTypes = Mapper.Map<ProductTypeDto, ProductType>(ProductTypeDtos);
            ProductTypeDtos.status = true;
            _context.ProductTypes.Add(ProductTypes);
            _context.SaveChanges();

            ProductTypeDtos.id = ProductTypes.id;

            return Created(new Uri(Request.RequestUri + "/" + ProductTypeDtos.id), ProductTypeDtos);
        }

        [HttpPut]
        //PUT : /api/ProductTypes/{id}
        public IHttpActionResult EditProductTypes(int id, ProductTypeDto ProductTypeDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.ProductTypes.SingleOrDefault(c => c.producttypename == ProductTypeDtos.producttypename && c.id != ProductTypeDtos.id);
            if (isExist != null)
                return BadRequest();

            var ProductTypesInDb = _context.ProductTypes.SingleOrDefault(c => c.id == id);
            Mapper.Map(ProductTypeDtos, ProductTypesInDb);
            _context.SaveChanges();

            return Ok(ProductTypeDtos);
        }

        [HttpDelete]
        //PUT : /api/ProductTypes/{id}
        public IHttpActionResult DeleteProductTypes(int id)
        {

            var ProductTypesInDb = _context.ProductTypes.SingleOrDefault(c => c.id == id);
            if (ProductTypesInDb == null)
                return NotFound();
            _context.ProductTypes.Remove(ProductTypesInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
