﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Data.Entity;

namespace CamTojobs.Controllers.Api
{
    public class ProductsController : ApiController
    {
        private ApplicationDbContext _context;

        public ProductsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        //[Route("api/Product?departmentid={departmentid}")]
        //Get : api/Product

        public IHttpActionResult GetProducts(string showRoomId)
        {
            if (showRoomId == "All")
            {
                var getProducts = _context.Products
                                .Include(c => c.showroom).
                                Include(c=>c.producttype)
                                .Select(Mapper.Map<Product, ProductDto>);
                return Ok(getProducts);
            }

            else
            {
                var getProducts = _context.Products
                                 .Include(c => c.showroom)
                                 .Include(c => c.producttype)
                                 .Select(Mapper.Map<Product, ProductDto>)
                                 .Where(c => c.showroomid == int.Parse(showRoomId));

                return Ok(getProducts);
            }

        }


        [HttpGet]
        //[Route("api/Products/{id}")]
        //Get : api/Products{id}
        public IHttpActionResult GetProductsID(int id)
        {
            var getProductsById = _context.Products.SingleOrDefault(c => c.id == id);

            if (getProductsById == null)
                return NotFound();

            return Ok(Mapper.Map<Product, ProductDto>(getProductsById));
        }
        [HttpPost]
        //[Route("api/Products")]
        public IHttpActionResult CreateProducts()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //var id = HttpContext.Current.Request.Form["Id"];
            var productname = HttpContext.Current.Request.Form["productname"];
            var producttypeid = HttpContext.Current.Request.Form["producttypeid"];
            var qtyonhand = HttpContext.Current.Request.Form["qtyonhand"];
            var photo = HttpContext.Current.Request.Files["photo"];
            var showroomid = HttpContext.Current.Request.Form["showroomid"];
            var createby = User.Identity.GetUserName();
            var createdate = DateTime.Today;

            var proInDb = _context.Products.SingleOrDefault(c => c.productname == productname && c.status == true);

            
            var productDto = new ProductDto()
            {
                productname = productname,
                producttypeid = int.Parse(producttypeid),
                qtyonhand = decimal.Parse(qtyonhand),
                showroomid = int.Parse(showroomid),
                createby = createby,
                createdate = createdate,
                status = true,
            };
            var product = Mapper.Map<ProductDto, Product>(productDto);
            _context.Products.Add(product);
            _context.SaveChanges();

            productDto.id = product.id;

            return Created(new Uri(Request.RequestUri + "/" + productDto.id), productDto);
        }

        [HttpPut]
        //[Route("api/Products/{id}")]
        //PUT : /api/Products/{id}
        public IHttpActionResult EditProducts(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var productname = HttpContext.Current.Request.Form["productname"];
            var producttypeid = HttpContext.Current.Request.Form["producttypeid"];
            var qtyonhand = HttpContext.Current.Request.Form["qtyonhand"];
            var photo = HttpContext.Current.Request.Files["photo"];
            var showroomid = HttpContext.Current.Request.Form["showroomid"];
            var createby = User.Identity.GetUserName();
            var createdate = DateTime.Today;

            var proInDb = _context.Products.SingleOrDefault(c => c.id == id/* && c.status == true*/);



            
                var productDto = new ProductDto()
                {
                    id = id,
                    productname = productname,
                    producttypeid = int.Parse(producttypeid),
                    qtyonhand = decimal.Parse(qtyonhand),
                    showroomid = int.Parse(showroomid),
                    createby = createby,
                    createdate = createdate,
                    status = true,
                };
                Mapper.Map(productDto, proInDb);
                _context.SaveChanges();
            
            
            return Ok(new { });
        }

        [HttpDelete]
        [Route("api/Products/{id}")]
        //PUT : /api/Products/{id}
        public IHttpActionResult DeleteProducts(int id)
        {
            var ProductsInDb = _context.Products.SingleOrDefault(c => c.id == id);
            if (ProductsInDb == null)
                return NotFound();
            _context.Products.Remove(ProductsInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
