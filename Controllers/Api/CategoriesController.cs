﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;

namespace CamTojobs.Controllers.Api
{
    [Authorize]
    public class CategoriesController : ApiController
    {
        private ApplicationDbContext _context;

        public CategoriesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        //Get : api/Categories
        public IHttpActionResult GetCategory()
        {
            var getCategory = _context.Categories.ToList().Select(Mapper.Map<Category, CategoryDto>);

            return Ok(new
            {
                code = 200,
                message = "Data Successfully",
                data = getCategory
            });
        }


        [HttpGet]
        //Get : api/Categories{id}
        public IHttpActionResult GetCategory(int id)
        {
            var getCategoryById = _context.Categories.SingleOrDefault(c => c.id == id);

            if (getCategoryById == null)
                return NotFound();

            return Ok(Mapper.Map<Category, CategoryDto>(getCategoryById));
        }

        public IHttpActionResult CreateCategory(CategoryDto CategoryDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Categories.SingleOrDefault(c => c.categoryName == CategoryDtos.categoryName);
            if (isExist != null)
                return BadRequest();

            var Category = Mapper.Map<CategoryDto, Category>(CategoryDtos);

            _context.Categories.Add(Category);
            _context.SaveChanges();

            CategoryDtos.id = Category.id;

            return Created(new Uri(Request.RequestUri + "/" + CategoryDtos.id), CategoryDtos);
        }

        [HttpPut]
        //PUT : /api/Category/{id}
        public IHttpActionResult EditCategory(int id, CategoryDto CategoryDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.Categories.SingleOrDefault(c => c.categoryName == CategoryDtos.categoryName && c.id != CategoryDtos.id);
            if (isExist != null)
                return BadRequest();

            var CategoryInDb = _context.Categories.SingleOrDefault(c => c.id == id);
            Mapper.Map(CategoryDtos, CategoryInDb);
            _context.SaveChanges();

            return Ok(CategoryDtos);
        }

        [HttpDelete]
        //PUT : /api/Categories/{id}
        public IHttpActionResult DeleteCategory(int id)
        {

            var CategoryInDb = _context.Categories.SingleOrDefault(c => c.id == id);
            if (CategoryInDb == null)
                return NotFound();
            _context.Categories.Remove(CategoryInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
