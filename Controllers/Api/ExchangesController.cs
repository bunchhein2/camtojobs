﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class ExchangesController : ApiController
    {
        private ApplicationDbContext _context;

        public ExchangesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/Exchanges/{a}/{b}")]
        public IHttpActionResult GetLastExchange(int a, int b)
        {
            var exchageRates = _context.Exchanges.OrderByDescending(c => c.rateid).FirstOrDefault(c => c.IsDeleted == false);
            return Ok(exchageRates);
        }
        [HttpGet]
        //Get : api/Exchanges
        [Route("api/Exchanges")]
        public IHttpActionResult GetExchanges()
        {
            var getExchanges = _context.Exchanges.ToList().Select(Mapper.Map<Exchange, ExchangeDto>).Where(c=>c.IsDeleted==false);

            return Ok(getExchanges);
        }


        [HttpGet]
        //Get : api/Exchanges{id}
        [Route("api/Exchanges/{id}")]
        public IHttpActionResult GetExchangesId(int id)
        {
            var getExchangesById = _context.Exchanges.SingleOrDefault(c => c.rateid == id && c.IsDeleted == false);

            if (getExchangesById == null)
                return NotFound();

            return Ok(Mapper.Map<Exchange, ExchangeDto>(getExchangesById));
        }
        [HttpPost]
        [Route("api/Exchanges")]
        public IHttpActionResult CreateExchanges(ExchangeDto ExchangeDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Exchanges.SingleOrDefault(c => c.rate == ExchangeDtos.rate);
            if (isExist != null)
                return BadRequest();

            var Exchanges = Mapper.Map<ExchangeDto, Exchange>(ExchangeDtos);
            ExchangeDtos.status = true;
            ExchangeDtos.date = DateTime.Today;
            _context.Exchanges.Add(Exchanges);
            _context.SaveChanges();

            ExchangeDtos.rateid = Exchanges.rateid;

            return Created(new Uri(Request.RequestUri + "/" + ExchangeDtos.rateid), ExchangeDtos);
        }

        [HttpPut]
        [Route("api/Exchanges/{id}")]
        //PUT : /api/ProductTypes/{id}
        public IHttpActionResult EditExchanges(int id, ExchangeDto ExchangeDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.Exchanges.SingleOrDefault(c => c.rateid == ExchangeDtos.rate && c.rate != ExchangeDtos.rateid);
            if (isExist != null)
                return BadRequest();

            var ExchangesInDb = _context.Exchanges.SingleOrDefault(c => c.rateid == id);
            ExchangeDtos.date = DateTime.Today;
            Mapper.Map(ExchangeDtos, ExchangesInDb);
            _context.SaveChanges();

            return Ok(ExchangeDtos);
        }

        [HttpDelete]
        [Route("api/Exchanges/{id}")]
        //PUT : /api/Exchanges/{id}
        public IHttpActionResult DeleteExchanges(int id)
        {

            var ExchangesInDb = _context.Exchanges.SingleOrDefault(c => c.rateid == id);
            if (ExchangesInDb == null)
                return NotFound();
            ExchangesInDb.IsDeleted = true;
           // _context.Exchanges.Remove(ExchangesInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
