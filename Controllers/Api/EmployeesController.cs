﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Data.Entity;


namespace CamTojobs.Controllers.Api
{
    [Authorize]
    public class EmployeesController : ApiController
    {
        private ApplicationDbContext _context;

        public EmployeesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        //[Route("api/Employees?departmentid={departmentid}")]
        //Get : api/Employees

        public IHttpActionResult GetEmployee(string departmentid)
        {
            if (departmentid == "All")
            {
                var getVacancy = _context.Employees
                                .Include(c => c.Department)
                                .Include(c => c.Position)
                                .Include(c => c.ShowRoom)
                                .Select(Mapper.Map<Employee, EmplyeeDto>);
                return Ok(getVacancy);
            }

            else
            {
                var getVacancy = _context.Employees
                                .Include(c => c.Department)
                                .Include(c => c.Position)
                                .Include(c => c.ShowRoom)
                                .Select(Mapper.Map<Employee, EmplyeeDto>)
                                .Where(c => c.departmentid == int.Parse(departmentid));
                return Ok(getVacancy);
            }
            
        }


        [HttpGet]
        //[Route("api/Employees/{id}")]
        //Get : api/Employees{id}
        public IHttpActionResult GetEmployeeID(int id)
        {
            var getEmployeeById = _context.Employees.SingleOrDefault(c => c.id == id);

            if (getEmployeeById == null)
                return NotFound();

            return Ok(Mapper.Map<Employee, EmplyeeDto>(getEmployeeById));
        }
        [HttpPost]
        //[Route("api/Employees")]
        public IHttpActionResult CreateEmployee()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //var id = HttpContext.Current.Request.Form["Id"];
            var name = HttpContext.Current.Request.Form["name"];
            var namekh = HttpContext.Current.Request.Form["namekh"];
            var sex = HttpContext.Current.Request.Form["sex"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var dob = HttpContext.Current.Request.Form["dob"];
            var address = HttpContext.Current.Request.Form["address"];
            var email = HttpContext.Current.Request.Form["email"];
            var positionid = HttpContext.Current.Request.Form["positionid"];
            var photo = HttpContext.Current.Request.Files["photo"];
            var identityno = HttpContext.Current.Request.Form["identityno"];
            var shippertype = HttpContext.Current.Request.Form["shippertype"];
            var vehicle = HttpContext.Current.Request.Form["vehicle"];
            var plateno = HttpContext.Current.Request.Form["plateno"];
            var showroomid = HttpContext.Current.Request.Form["showroomid"];
            var departmentid = HttpContext.Current.Request.Form["departmentid"];
            var phonecard = HttpContext.Current.Request.Form["phonecard"];
            var petroluem = HttpContext.Current.Request.Form["petoluem"];
            var deliveryin = HttpContext.Current.Request.Form["deliveryin"];
            var deliveryout = HttpContext.Current.Request.Form["deliveryout"];
            var createby = User.Identity.GetUserName();
            var createdate = DateTime.Today;

            var empInDb = _context.Employees.SingleOrDefault(c => c.name == name && c.status == true);

            if (empInDb != null)
                return BadRequest();

            string photoName = "";
            if (photo != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(photo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(photo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(photo.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                photo.SaveAs(fileSavePath);

            }

            var employeeDto = new EmplyeeDto()
            {
                name = name,
                namekh = namekh,
                sex = sex,
                phone = phone,
                dob = DateTime.Parse(dob),
                address = address,
                email = email,
                positionid = int.Parse(positionid),
                photo = photoName,
                identityno = identityno,
                shippertype = shippertype,
                vehicle = vehicle,
                plateno = plateno,
                createby = createby,
                createdate = createdate,
                status = true,
                showroomid = int.Parse(showroomid),
                departmentid = int.Parse(departmentid),
                phonecard = decimal.Parse(phonecard),
                petoluem = decimal.Parse(petroluem),
                deliveryin = decimal.Parse(deliveryin),
                deliveryout = decimal.Parse(deliveryout)
            };
            var employee = Mapper.Map<EmplyeeDto, Employee>(employeeDto);
            _context.Employees.Add(employee);
            _context.SaveChanges();

            employeeDto.id = employee.id;

            return Created(new Uri(Request.RequestUri + "/" + employeeDto.id), employeeDto);
        }

        [HttpPut]
        //[Route("api/Employees/{id}")]
        //PUT : /api/Employees/{id}
        public IHttpActionResult EditShowRoom(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var name = HttpContext.Current.Request.Form["name"];
            var namekh = HttpContext.Current.Request.Form["namekh"];
            var sex = HttpContext.Current.Request.Form["sex"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var dob = HttpContext.Current.Request.Form["dob"];
            var address = HttpContext.Current.Request.Form["address"];
            var email = HttpContext.Current.Request.Form["email"];
            var positionid = HttpContext.Current.Request.Form["positionid"];
            var photo = HttpContext.Current.Request.Files["photo"];
            var identityno = HttpContext.Current.Request.Form["identityno"];
            var shippertype = HttpContext.Current.Request.Form["shippertype"];
            var vehicle = HttpContext.Current.Request.Form["vehicle"];
            var plateno = HttpContext.Current.Request.Form["plateno"];
            var showroomid = HttpContext.Current.Request.Form["showroomid"];
            var departmentid = HttpContext.Current.Request.Form["departmentid"];
            var phonecard = HttpContext.Current.Request.Form["phonecard"];
            var petroluem = HttpContext.Current.Request.Form["petoluem"];
            var deliveryin = HttpContext.Current.Request.Form["deliveryin"];
            var deliveryout = HttpContext.Current.Request.Form["deliveryout"];
            var createby = User.Identity.GetUserName();
            var createdate = DateTime.Today;

            var empInDb = _context.Employees.SingleOrDefault(c => c.id == id/* && c.status == true*/);

            

            string photoName = "";
            if (photo != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(photo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(photo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(photo.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                photo.SaveAs(fileSavePath);

                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), empInDb.photo);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
                var employeeDto = new EmplyeeDto()
                {
                    id = id,
                    name = name,
                    namekh = namekh,
                    sex = sex,
                    phone = phone,
                    dob = DateTime.Parse(dob),
                    address = address,
                    email = email,
                    positionid = int.Parse(positionid),
                    photo = photoName,
                    identityno = identityno,
                    shippertype = shippertype,
                    vehicle = vehicle,
                    plateno = plateno,
                    createby = createby,
                    createdate = createdate,
                    status = true,
                    showroomid = int.Parse(showroomid),
                    departmentid = int.Parse(departmentid),
                    phonecard = decimal.Parse(phonecard),
                    petoluem = decimal.Parse(petroluem),
                    deliveryin = decimal.Parse(deliveryin),
                    deliveryout = decimal.Parse(deliveryout)
                };
                Mapper.Map(employeeDto, empInDb);
                _context.SaveChanges();
            }
            else
            {
                var employeeDto = new EmplyeeDto()
                {
                    id = id,
                    name = name,
                    namekh = namekh,
                    sex = sex,
                    phone = phone,
                    dob = DateTime.Parse(dob),
                    address = address,
                    email = email,
                    positionid = int.Parse(positionid),
                    photo = empInDb.photo,
                    identityno = identityno,
                    shippertype = shippertype,
                    vehicle = vehicle,
                    plateno = plateno,
                    createby = createby,
                    createdate = createdate,
                    status = true,
                    showroomid = int.Parse(showroomid),
                    departmentid = int.Parse(departmentid),
                    phonecard = decimal.Parse(phonecard),
                    petoluem = decimal.Parse(petroluem),
                    deliveryin = decimal.Parse(deliveryin),
                    deliveryout = decimal.Parse(deliveryout)
                };
                Mapper.Map(employeeDto, empInDb);
                _context.SaveChanges();
            }
            return Ok(new { });
        }
        
        [HttpDelete]
        [Route("api/Employees/{id}")]
        //PUT : /api/Employees/{id}
        public IHttpActionResult DeleteEmployee(int id)
        {
            var EmployeeInDb = _context.Employees.SingleOrDefault(c => c.id == id);
            if (EmployeeInDb == null)
                return NotFound();
            _context.Employees.Remove(EmployeeInDb);
            _context.SaveChanges();

            return Ok(new { });
        }

    }
}       

