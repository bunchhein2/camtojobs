﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;

namespace CamTojobs.Controllers.Api
{
    [Authorize]
    public class CompaniesController : ApiController
    {
        private ApplicationDbContext _context = new ApplicationDbContext();


        [HttpGet]
        // GET: api/Companies
        public IHttpActionResult GetCompany()
        {
            var getCompany = _context.companies.Include(c => c.CompanyType).ToList().Select(Mapper.Map<Company, CompanyDtos>).Where(c => c.IsDeleted == false);
            return Ok(getCompany);
        }

        [HttpGet]
        //GET : api/Companies/{id}
        public IHttpActionResult GetCompanyById(int id)
        {
            var getCompanyById = _context.companies.Include(c => c.CompanyType).SingleOrDefault(c => c.Id == id && c.IsDeleted == false);

            if (getCompanyById == null)
                return BadRequest();

            return Ok(Mapper.Map<Company, CompanyDtos>(getCompanyById));
        }

        //POST : api/Companies
        public IHttpActionResult CreateCompany()
        {
            var id = HttpContext.Current.Request.Form["ID"];
            var companyName = HttpContext.Current.Request.Form["CompanyName"];
            var companyTypeId = HttpContext.Current.Request.Form["CompanyTypeId"];
            var contactPerson = HttpContext.Current.Request.Form["ContactPerson"];
            var contactPhone = HttpContext.Current.Request.Form["ContactPhone"];
            var email = HttpContext.Current.Request.Form["Email"];
            var address = HttpContext.Current.Request.Form["Address"];
            var photo = HttpContext.Current.Request.Files["Photo"];

            //Check if not null
            //var isExist = _context.companies.SingleOrDefault(c => c.CompanyName == companyName && c.IsDeleted == false);
            //if (isExist != null)
              //  return BadRequest();

            string photoName = "";

            if(photo != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(photo.FileName)
                    ,string.Concat(Path.GetFileNameWithoutExtension(photo.FileName)
                    ,DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    ,Path.GetExtension(photo.FileName)                    
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                photo.SaveAs(savePath);
            }
            var companyDtos = new CompanyDtos()
            {
                //Id = Int32.Parse(id),
                CompanyName = companyName,
                CompanyTypeId = Int32.Parse(companyTypeId),
                ContactPerson = contactPerson,
                ContactPhone = Int32.Parse(contactPhone),
                Email = email,
                Address = address,
                Photo = photoName
            };
            var company = Mapper.Map<CompanyDtos, Company>(companyDtos);
            _context.companies.Add(company);
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + companyDtos.Id), companyDtos);
        }

        //PUT : api/Companies/{id}
        [HttpPut]
        public IHttpActionResult UpdateCompany()
        {
            var id = HttpContext.Current.Request.Form["Id"];
            var companyName = HttpContext.Current.Request.Form["CompanyName"];
            var companyTypeId = HttpContext.Current.Request.Form["CompanyTypeId"];
            var contactPerson = HttpContext.Current.Request.Form["ContactPerson"];
            var contactPhone = HttpContext.Current.Request.Form["ContactPhone"];
            var email = HttpContext.Current.Request.Form["Email"];
            var address = HttpContext.Current.Request.Form["Address"];
            var photo = HttpContext.Current.Request.Files["Photo"];
            int companyId = 0;
            if (id != null)
            {
                companyId = int.Parse(id);
            }

            var isExist = _context.companies.SingleOrDefault(c => c.Id == companyId ); //&& c.IsDeleted == false
            //if (isExist != null)
            //    return BadRequest();

            string photoName = "";

            if (photo != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(photo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(photo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(photo.FileName)
                    ));
                var photoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                photo.SaveAs(photoPath);

                //Delete Old Photo path
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), isExist.Photo);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }

                var companyDtos = new CompanyDtos()
                {
                    Id = Int32.Parse(id),
                    CompanyName = companyName,
                    CompanyTypeId = Int32.Parse(companyTypeId),
                    ContactPerson = contactPerson,
                    ContactPhone = Int32.Parse(contactPhone),
                    Email = email,
                    Address = address,
                    Photo = photoName
                };

                Mapper.Map(companyDtos, isExist);
                _context.SaveChanges();
            }
            else
            {
                var companyDtos = new CompanyDtos()
                {
                    Id = Int32.Parse(id),
                    CompanyName = companyName,
                    CompanyTypeId = Int32.Parse(companyTypeId),
                    ContactPerson = contactPerson,
                    ContactPhone = Int32.Parse(contactPhone),
                    Email = email,
                    Address = address,
                    Photo = isExist.Photo
                };

                Mapper.Map(companyDtos, isExist);
                _context.SaveChanges();
            }
            

            return Ok(new { });
        }

        //DELETE : api/Companies/{id}
        public IHttpActionResult DeleteCompany(int id)
        {
            var companyInDb = _context.companies.SingleOrDefault(c => c.Id == id);
            if (companyInDb == null)
                return NotFound();
            companyInDb.IsDeleted = true;
            _context.SaveChanges();


            //var photoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), companyInDb.Photo);
            //if(File.Exists(photoPath))
            //{
            //    File.Delete(photoPath);
            //}


            return Ok(new { });
        }
    }
}