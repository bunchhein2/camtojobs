﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class InvoicesController : ApiController
    {
        private ApplicationDbContext _context;

        public InvoicesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/Invoices")]
        //Get : api/Invoices
        public IHttpActionResult GetInvoices()
        {
            var getInvoices = from i in _context.Invoice
                              join c in _context.Customers on i.customerid equals c.id
                              join s in _context.ShowRooms on i.showroomid equals s.id
                              join e in _context.Exchanges on i.exchangeid equals e.rateid
                              select new
                              {
                                  date = i.date,
                                  invoiceno = i.invoiceno,
                                  customerid = i.customerid,
                                  customername=c.name,
                                  showroomid = i.showroomid,
                                  showroom=s.name,
                                  exchangeid=i.exchangeid,
                                  exchangerate=e.rate,
                                  totalamount=i.totalamount,
                                  totalcarprice=i.totalcarprice,
                                  totalshipprice=i.totalshipprice,
                                  alreadypaid=i.alreadypaid,
                                  status=i.status,
                                  createby=i.createby,
                                  createdate=i.createdate,
                                  paid=i.paid
                              };
            return Ok(getInvoices);
        }
        [HttpGet]
        [Route("api/Invoices/{id}")]
        //Get : api/Invoices{id}
        public IHttpActionResult GetInvoicesId(int id)
        {
            //var getInvoiceById = _context.Invoices.SingleOrDefault(c => c.id == id);

            //if (getInvoiceById == null)
            //    return NotFound();

            //return Ok(Mapper.Map<Invoice, InvoiceDto>(getInvoiceById));
            var getInvoices = from i in _context.Invoice
                              join c in _context.Customers on i.customerid equals c.id
                              join s in _context.ShowRooms on i.showroomid equals s.id
                              join e in _context.Exchanges on i.exchangeid equals e.rateid
                              where i.id == id
                              select new
                              {
                                  date = i.date,
                                  invoiceno = i.invoiceno,
                                  customerid = i.customerid,
                                  customername = c.name,
                                  showroomid = i.showroomid,
                                  showroom = s.name,
                                  exchangeid = i.exchangeid,
                                  exchangerate = e.rate,
                                  totalamount = i.totalamount,
                                  totalcarprice = i.totalcarprice,
                                  totalshipprice = i.totalshipprice,
                                  alreadypaid = i.alreadypaid,
                                  status = i.status,
                                  createby = i.createby,
                                  createdate = i.createdate,
                                  paid = i.paid
                              };
            return Ok(getInvoices);
        }
        [HttpGet]
        [Route("api/InvoicesNoMax")]
        public IHttpActionResult GetInvNoMaxID()
        {
            //For Get Max PaymentNo +1
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT CASE WHEN MAX(invoiceno) IS NULL THEN (1)ELSE MAX(invoiceno)+1 END AS ID,'DE' + RIGHT('000000' + CONVERT(NVARCHAR, (SELECT CASE WHEN MAX(invoiceno) IS NULL THEN (1)ELSE MAX(invoiceno)+1 END )) , 6) AS InvoiceID FROM invoice_tbl", conx);
            adp.Fill(ds);
            string InvNo = ds.Rows[0][0].ToString();
            string InvNoFormat = ds.Rows[0][1].ToString();
            return Ok(InvNo + "," + InvNoFormat);
        }
        [HttpPost]
        [Route("api/Invoices")]
        public IHttpActionResult CreateInvoices(InvoiceDto InvoiceDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var invoices = Mapper.Map<InvoiceDto, Invoice>(InvoiceDtos);
            InvoiceDtos.status = true;
            InvoiceDtos.createby = User.Identity.GetUserName();
            InvoiceDtos.createdate = DateTime.Today;
            _context.Invoice.Add(invoices);
            _context.SaveChanges();

            InvoiceDtos.id = invoices.id;

            return Created(new Uri(Request.RequestUri + "/" + InvoiceDtos.id), InvoiceDtos);
        }
        [HttpPut]
        [Route("api/Invoices/{id}")]
        public IHttpActionResult EditInvoices(int id, InvoiceDto InvoiceDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var invoices =_context.Invoice.SingleOrDefault(c=>c.id==id);
            Mapper.Map(InvoiceDtos, invoices);
            _context.SaveChanges();

            return Ok(InvoiceDtos);
        }
        [HttpDelete]
        [Route("api/Invoices/{id}")]
        //PUT : /api/Invoices/{id}
        public IHttpActionResult DeleteInvoices(int id)
        {

            var invoicesInDb = _context.Invoice.SingleOrDefault(c => c.id == id);
            if (invoicesInDb == null)
                return NotFound();
            _context.Invoice.Remove(invoicesInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
