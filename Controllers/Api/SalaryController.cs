﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class SalaryController : ApiController
    {
        private ApplicationDbContext _context;

        public SalaryController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        //Get : api/Salary

        public IHttpActionResult GetSalary()
        {
            var getSalary = from s in _context.Salary
                            join e in _context.Employees on s.employeeid equals e.id
                            select new
                            {
                                id = s.id,
                                date = s.date,
                                employeename = e.name,
                                amount = s.amount,
                                createby = s.createby,
                                createdate = s.createdate
                            };

            return Ok(getSalary);
        }
        [HttpGet]
        //Get : api/Salary/{id}
        [Route("api/Salary/{id}")]
        public IHttpActionResult GetSalaryId(int id)
        {
            var getSalary = _context.Salary.SingleOrDefault(c => c.id == id);

            return Ok(Mapper.Map<Salary,SalaryDto>(getSalary));
        }
        [HttpGet]
        //Get : api/Salary/{id}
        [Route("api/SalaryByEmp/{id}/{a}")]
        public IHttpActionResult GetSalaryIdEmp(int id)
        {
            var getSalary = _context.Salary.Where(c => c.employeeid == id);

            return Ok(getSalary);
        }
        [HttpPost]
        //Get : api/Salary/{id}
        [Route("api/Salary")]
        public IHttpActionResult CreateSalary(SalaryDto salaryDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            
            var salary = Mapper.Map<SalaryDto, Salary>(salaryDto);
            salaryDto.createdate = DateTime.Today;
            salaryDto.createby = User.Identity.GetUserName();
            _context.Salary.Add(salary);
            _context.SaveChanges();

            salaryDto.id = salary.id;

            return Created(new Uri(Request.RequestUri + "/" + salaryDto.id), salaryDto);
        }
        [HttpPut]
        //PUT : /api/Salary/{id}
        [Route("api/Salary/{id}")]
        public IHttpActionResult EditSalary(int id, SalaryDto salaryDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var salaryInDb = _context.Salary.SingleOrDefault(c => c.id == id);
            salaryDto.createby = User.Identity.GetUserName();
            Mapper.Map(salaryDto, salaryInDb);
            _context.SaveChanges();

            return Ok(salaryDto);
        }
        [HttpDelete]
        [Route("api/Salary/{id}")]

        public IHttpActionResult RemoveSalary(int id)
        {
            var salaryInDb = _context.Salary.SingleOrDefault(c => c.id == id);
            _context.Salary.Remove(salaryInDb);
            _context.SaveChanges();
            return Ok(new { });
        }
    }
}
