﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using System.Data.Entity;

namespace CamTojobs.Controllers.Api
{
    [Authorize]
    public class VacanciesController : ApiController
    {
        private ApplicationDbContext _context;

        public VacanciesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        //Get : api/Vacancies?companyId={companyId}
        public IHttpActionResult GetVacancy(string companyId)
        {

            if(companyId == "All")
            {
                var getVacancy = _context.Vacancies
                                .Include(c => c.Company)
                                .Include(c => c.Position)
                                .Include(c => c.Category)
                                .Select(Mapper.Map<Vacancy, VacancyDto>);
                return Ok(getVacancy);
            }

            else
            {
                var getVacancy = _context.Vacancies
                                .Include(c => c.Company)
                                .Include(c => c.Position)
                                .Include(c => c.Category)
                                .Select(Mapper.Map<Vacancy, VacancyDto>)
                                .Where(c => c.CompanyId == int.Parse(companyId));
                return Ok(getVacancy);
            }

           
        }


        [HttpGet]
        //Get : api/Vacancies{id}
        public IHttpActionResult GetVacancy(int id)
        {
            var getVacancyById = _context.Vacancies.SingleOrDefault(c => c.Id == id);

            if (getVacancyById == null)
                return NotFound();

            return Ok(Mapper.Map<Vacancy, VacancyDto>(getVacancyById));
        }
        [HttpPost]
        public IHttpActionResult CreateVacancy(VacancyDto VacancyDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //var isExist = _context.Vacancies.SingleOrDefault(c => c.VacancyName == VacancyDtos.VacancyName);
            //if (isExist != null)
            //    return BadRequest();

            var Vacancy = Mapper.Map<VacancyDto, Vacancy>(VacancyDtos);

            _context.Vacancies.Add(Vacancy);
            _context.SaveChanges();

            VacancyDtos.Id = Vacancy.Id;

            return Created(new Uri(Request.RequestUri + "/" + VacancyDtos.Id), VacancyDtos);
        }

        [HttpPut]
        //PUT : /api/Vacancy/{id}
        public IHttpActionResult EditVacancy(int id, VacancyDto VacancyDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            //var isExist = _context.Vacancies.SingleOrDefault(c => c.VacancyName == VacancyDtos.VacancyName && c.id != VacancyDtos.id);
            //if (isExist != null)
            //    return BadRequest();

            var VacancyInDb = _context.Vacancies.SingleOrDefault(c => c.Id == id);
            Mapper.Map(VacancyDtos, VacancyInDb);
            _context.SaveChanges();

            return Ok(VacancyDtos);
        }

        [HttpDelete]
        //PUT : /api/Vacancies/{id}
        public IHttpActionResult DeleteVacancy(int id)
        {

            var VacancyInDb = _context.Vacancies.SingleOrDefault(c => c.Id == id);
            if (VacancyInDb == null)
                return NotFound();
            _context.Vacancies.Remove(VacancyInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
