﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class Invoices1Controller : ApiController
    {
        private ApplicationDbContext _context;

        public Invoices1Controller()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/Invoices1/{date}/{a}")]
        //Get : api/Invoices
        public IHttpActionResult GetInvoices1(string date,string a)
        {
            if (date == "all")
            {
                var getInvoices = from i in _context.Invoices
                                  join c in _context.Customers on i.customerid equals c.id
                                  join s in _context.ShowRooms on i.showroomid equals s.id
                                  join e in _context.Exchanges on i.exchangeid equals e.rateid
                                  select new Invoices1V
                                  {
                                      id = i.id,
                                      date = i.date,
                                      invoiceno = i.invoiceno,
                                      customerid = i.customerid,
                                      customername = c.name,
                                      showroomid = i.showroomid,
                                      showroomname = s.name,
                                      exchangeid = i.exchangeid,
                                      rate = e.rate,
                                      totalamount = i.totalamount,
                                      totalcarprice = i.totalcarprice,
                                      totalshipprice = i.totalshipprice,
                                      alreadypaid = i.alreadypaid,
                                      status = i.status,
                                      createby = i.createby,
                                      createdate = i.createdate,
                                      paid = i.paid
                                  };
                return Ok(getInvoices);
            }
            else
            {
                var date1 = DateTime.Parse(date);
                var getInvoices = from i in _context.Invoices
                                  join c in _context.Customers on i.customerid equals c.id
                                  join s in _context.ShowRooms on i.showroomid equals s.id
                                  join e in _context.Exchanges on i.exchangeid equals e.rateid
                                  where i.date == date1
                                  select new Invoices1V
                                  {
                                      id = i.id,
                                      date = i.date,
                                      invoiceno = i.invoiceno,
                                      customerid = i.customerid,
                                      customername = c.name,
                                      showroomid = i.showroomid,
                                      showroomname = s.name,
                                      exchangeid = i.exchangeid,
                                      rate = e.rate,
                                      totalamount = i.totalamount,
                                      totalcarprice = i.totalcarprice,
                                      totalshipprice = i.totalshipprice,
                                      alreadypaid = i.alreadypaid,
                                      status = i.status,
                                      createby = i.createby,
                                      createdate = i.createdate,
                                      paid = i.paid
                                  };
                return Ok(getInvoices);
            }
            
        }
        [HttpGet]
        [Route("api/Invoices1/{id}")]
        //Get : api/Invoices{id}
        public IHttpActionResult GetInvoicesId(int id)
        {
            var getInvoiceById = _context.Invoices.SingleOrDefault(c => c.id == id);

            if (getInvoiceById == null)
                return NotFound();

            return Ok(Mapper.Map<Invoices1, Invoices1Dto>(getInvoiceById));
        }
        [HttpPost]
        [Route("api/Invoices1")]
        public IHttpActionResult CreateInvoices(Invoices1Dto InvoiceDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var invoices = Mapper.Map<Invoices1Dto, Invoices1>(InvoiceDtos);
            invoices.status = true;
            invoices.createby = User.Identity.GetUserName();
            invoices.createdate = DateTime.Today;
            _context.Invoices.Add(invoices);
            _context.SaveChanges();

            InvoiceDtos.id = invoices.id;

            return Created(new Uri(Request.RequestUri + "/" + InvoiceDtos.id), InvoiceDtos);
        }
        [HttpPut]
        [Route("api/Invoices1/{id}")]
        public IHttpActionResult EditInvoices(int id, Invoices1Dto InvoiceDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var invoices = _context.Invoices.SingleOrDefault(c => c.id == id);
            InvoiceDtos.status = true;
            InvoiceDtos.createby = User.Identity.GetUserName();
            InvoiceDtos.createdate = DateTime.Today;
            Mapper.Map(InvoiceDtos, invoices);
            _context.SaveChanges();

            return Ok(InvoiceDtos);
        }
        [HttpDelete]
        [Route("api/Invoices1/{id}")]
        //PUT : /api/Invoices/{id}
        public IHttpActionResult DeleteInvoices(int id)
        {

            var invoicesInDb = _context.Invoices.SingleOrDefault(c => c.id == id);
            if (invoicesInDb == null)
                return NotFound();
            _context.Invoices.Remove(invoicesInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
