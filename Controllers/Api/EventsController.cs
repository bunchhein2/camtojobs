﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;

namespace CamTojobs.Controllers.Api
{
    [Authorize]
    public class EventsController : ApiController
    {
        public ApplicationDbContext  _context;
        public EventsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();   
        }
        [HttpGet]
        [Route("api/events")]
        public IHttpActionResult GetEvent()
        {
            var getEvent = _context.Events.ToList().Select(Mapper.Map<Event, EventDto>);
            return Ok();
        }
        [HttpGet]
        [Route("api/events/{id}")]
        public IHttpActionResult GetEventById(int id)
        {
            var getEvent = _context.Events.ToList().SingleOrDefault(c => c.id == id);
            return Ok(Mapper.Map<Event, EventDto>(getEvent));
        }
        [HttpPost]
        [Route("api/events")]
        public IHttpActionResult CreateEvent(EventDto req)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            //var eventDto = new EventDto()
            //{
            //    //id = req.id,
            //    event_name=req.event_name,
            //    event_date=req.event_date,
            //    place=req.place,
            //    status=true
            //};

            var Event = Mapper.Map<EventDto, Event>(req);
            _context.Events.Add(Event);
            _context.SaveChanges();

            req.id = Event.id;

            return Created(new Uri(Request.RequestUri + "/" + req.id), req);
        }
        [HttpPut]
        [Route("api/events/{id}")]
        public IHttpActionResult UpdateEvent(EventDto req, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            //var eventDto = new EventDto()
            //{
            //    //id = req.id,
            //    event_name=req.event_name,
            //    event_date=req.event_date,
            //    place=req.place,
            //    status=true
            //};
            var eventInDb = _context.Events.SingleOrDefault(c => c.id == id);

            Mapper.Map(req, eventInDb);
            _context.SaveChanges();

            return Ok(req);
        }
        [HttpDelete]
        [Route("api/events/{id}")]
        public IHttpActionResult DeleteEvent(EventDto req, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            //var eventDto = new EventDto()
            //{
            //    //id = req.id,
            //    event_name=req.event_name,
            //    event_date=req.event_date,
            //    place=req.place,
            //    status=true
            //};
            var eventInDb = _context.Events.SingleOrDefault(c => c.id == id);
            if(eventInDb == null)
            {
                return NotFound();
            };
            _context.Events.Remove(eventInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
