﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;

namespace CamTojobs.Controllers.Api
{
    [Authorize]
    public class PositionsController : ApiController
    {
        private ApplicationDbContext _context;

        public PositionsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        //Get : api/Positions
        public IHttpActionResult GetPosition()
        {
            var getPosition = _context.Positions.ToList().Select(Mapper.Map<Position, PositionDto>);

            return Ok(getPosition);
        }


        [HttpGet]
        //Get : api/Positions{id}
        public IHttpActionResult GetPosition(int id)
        {
            var getPositionById = _context.Positions.SingleOrDefault(c => c.id == id);

            if (getPositionById == null)
                return NotFound();

            return Ok(Mapper.Map<Position, PositionDto>(getPositionById));
        }

        public IHttpActionResult CreatePosition(PositionDto PositionDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Positions.SingleOrDefault(c => c.positionName == PositionDtos.positionName);
            if (isExist != null)
                return BadRequest();

            var Position = Mapper.Map<PositionDto, Position>(PositionDtos);

            _context.Positions.Add(Position);
            _context.SaveChanges();

            PositionDtos.id = Position.id;

            return Created(new Uri(Request.RequestUri + "/" + PositionDtos.id), PositionDtos);
        }

        [HttpPut]
        //PUT : /api/Position/{id}
        public IHttpActionResult EditPosition(int id, PositionDto PositionDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.Positions.SingleOrDefault(c => c.positionName == PositionDtos.positionName && c.id != PositionDtos.id);
            if (isExist != null)
                return BadRequest();

            var PositionInDb = _context.Positions.SingleOrDefault(c => c.id == id);
            Mapper.Map(PositionDtos, PositionInDb);
            _context.SaveChanges();

            return Ok(PositionDtos);
        }

        [HttpDelete]
        //PUT : /api/Positions/{id}
        public IHttpActionResult DeletePosition(int id)
        {

            var PositionInDb = _context.Positions.SingleOrDefault(c => c.id == id);
            if (PositionInDb == null)
                return NotFound();
            _context.Positions.Remove(PositionInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
