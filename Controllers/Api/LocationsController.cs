﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class LocationsController : ApiController
    {
        private ApplicationDbContext _context;

        public LocationsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        //Get : api/Locations
        [Route("api/Locations")]
        public IHttpActionResult GetLocations()
        {
            var getLocations = _context.Locations.ToList().Select(Mapper.Map<Location, LocationDto>);

            return Ok(getLocations);
        }


        [HttpGet]
        //Get : api/Locations/{id}
        [Route("api/Locations/{id}")]
        public IHttpActionResult GetLocations(int id)
        {
            var getLocationsById = _context.Locations.SingleOrDefault(c => c.id == id);

            if (getLocationsById == null)
                return NotFound();

            return Ok(Mapper.Map<Location, LocationDto>(getLocationsById));
        }
        [HttpPost]
        [Route("api/Locations")]
        public IHttpActionResult CreateLocations(LocationDto LocationDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.Locations.SingleOrDefault(c => c.location == LocationDtos.location);
            if (isExist != null)
                return BadRequest();

            var Locations = Mapper.Map<LocationDto, Location>(LocationDtos);
            LocationDtos.status = true;
            _context.Locations.Add(Locations);
            _context.SaveChanges();

            LocationDtos.id = Locations.id;

            return Created(new Uri(Request.RequestUri + "/" + LocationDtos.id), LocationDtos);
        }

        [HttpPut]
        //PUT : /api/Locations/{id}
        [Route("api/Locations/{id}")]
        public IHttpActionResult EditLocations(int id, LocationDto LocationDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.Locations.SingleOrDefault(c => c.location == LocationDtos.location && c.id != LocationDtos.id);
            if (isExist != null)
                return BadRequest();

            var LocationsInDb = _context.Locations.SingleOrDefault(c => c.id == id);
            Mapper.Map(LocationDtos, LocationsInDb);
            _context.SaveChanges();

            return Ok(LocationDtos);
        }

        [HttpDelete]
        //PUT : /api/Locations/{id}
        [Route("api/Locations/{id}")]
        public IHttpActionResult DeleteLocations(int id)
        {

            var LocationsInDb = _context.Locations.SingleOrDefault(c => c.id == id);
            if (LocationsInDb == null)
                return NotFound();
            _context.Locations.Remove(LocationsInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
