﻿using AutoMapper;
using CamTojobs.App_Start;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class DepartmentsController : ApiController
    {
        private ApplicationDbContext _context;
        public DepartmentsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        public IHttpActionResult GetDepartment()
        {
            var getDepartment = _context.Departments.ToList().Select(Mapper.Map<Department, DepartmentDto>);
            return Ok(getDepartment);
        }
        [HttpGet]
        public IHttpActionResult GetDepartmentById(int id)
        {
            var getdepartmentbyid = _context.Departments.SingleOrDefault(c => c.id == id);
            return Ok(Mapper.Map<Department, DepartmentDto>(getdepartmentbyid));
        }
        [HttpPost]
        public IHttpActionResult CreateDepartment(DepartmentDto departmentdto)
        {

            if (!ModelState.IsValid)
                return BadRequest();

            var department = Mapper.Map<DepartmentDto, Department>(departmentdto);

            department.createby = User.Identity.GetUserId();
            department.createdate = DateTime.Today;
            _context.Departments.Add(department);
            _context.SaveChanges();

            departmentdto.id = department.id;

            return Created(new Uri(Request.RequestUri + "/" + departmentdto.id), departmentdto);
        }
        [HttpPut]
        public IHttpActionResult EditDepartment(int id, DepartmentDto departmentDto)
        {
            var department = _context.Departments.SingleOrDefault(c => c.id == id);
            Mapper.Map(departmentDto, department);
            _context.SaveChanges();

            return Ok(departmentDto);

        }
        [HttpDelete]
        public IHttpActionResult RemoveDepartment(int id)
        {
            var department = _context.Departments.SingleOrDefault(c => c.id == id);
            _context.Departments.Remove(department);
            _context.SaveChanges();
            return Ok(new { });
        }
    }
}
