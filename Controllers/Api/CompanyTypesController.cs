﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;

namespace CamTojobs.Controllers.Api
{
    [Authorize]
    public class CompanyTypesController : ApiController
    {
        private ApplicationDbContext _context;        

        public CompanyTypesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        
        [HttpGet]
        //Get : api/companyTypes
        public IHttpActionResult GetCompanyType()
        {
            var getCompanyType = _context.companyTypes.ToList().Select(Mapper.Map<CompanyType, CompanyTypeDtos>);

            return Ok(getCompanyType);
        }


        [HttpGet]
        //Get : api/companyTypes{id}
        public IHttpActionResult GetCompanyType(int id)
        {
            var getCompanyTypeById = _context.companyTypes.SingleOrDefault(c => c.Id == id);

            if (getCompanyTypeById == null)
                return NotFound();

            return Ok(Mapper.Map<CompanyType, CompanyTypeDtos>(getCompanyTypeById));
        }
        
        public IHttpActionResult CreateCompanyType(CompanyTypeDtos companyTypeDtos)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.companyTypes.SingleOrDefault(c => c.Name == companyTypeDtos.Name);
            if (isExist != null)
                return BadRequest();

            var companyType = Mapper.Map<CompanyTypeDtos, CompanyType>(companyTypeDtos);

            _context.companyTypes.Add(companyType);
            _context.SaveChanges();

            companyTypeDtos.Id = companyType.Id;

            return Created(new Uri(Request.RequestUri + "/"+ companyTypeDtos.Id),companyTypeDtos);
        }

        [HttpPut]
        //PUT : /api/companyType/{id}
        public IHttpActionResult EditCompanyType(int id, CompanyTypeDtos companyTypeDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.companyTypes.SingleOrDefault(c => c.Name == companyTypeDtos.Name && c.Id != companyTypeDtos.Id);
            if (isExist != null)
                return BadRequest();

            var companyTypeInDb = _context.companyTypes.SingleOrDefault(c => c.Id == id);
            Mapper.Map(companyTypeDtos, companyTypeInDb);
            _context.SaveChanges();

            return Ok(companyTypeDtos);
        }

        [HttpDelete]
        //PUT : /api/companyTypes/{id}
        public IHttpActionResult DeleteCompanyType(int id)
        {

            var companyTypeInDb = _context.companyTypes.SingleOrDefault(c => c.Id == id);
            if (companyTypeInDb == null)
                return NotFound();
            _context.companyTypes.Remove(companyTypeInDb);
            _context.SaveChanges();

            return Ok(new { });
        }

    }

}
