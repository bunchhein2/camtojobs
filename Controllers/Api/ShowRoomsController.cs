﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class ShowRoomsController : ApiController
    {
        private ApplicationDbContext _context;

        public ShowRoomsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        //Get : api/Positions
        public IHttpActionResult GetShowRoom()
        {
            var getShowRoom = _context.ShowRooms.ToList().Select(Mapper.Map<ShowRoom, ShowRoomDto>);

            return Ok(getShowRoom);
        }


        [HttpGet]
        //Get : api/Positions{id}
        public IHttpActionResult GetShowRoom(int id)
        {
            var getShowRoomById = _context.ShowRooms.SingleOrDefault(c => c.id == id);

            if (getShowRoomById == null)
                return NotFound();

            return Ok(Mapper.Map<ShowRoom, ShowRoomDto>(getShowRoomById));
        }
        [HttpPost]
        public IHttpActionResult CreateShowRoom(ShowRoomDto ShowRoomDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.ShowRooms.SingleOrDefault(c => c.name == ShowRoomDtos.name);
            if (isExist != null)
                return BadRequest();

            var ShowRoom = Mapper.Map<ShowRoomDto, ShowRoom>(ShowRoomDtos);
            ShowRoomDtos.status = true;
            ShowRoomDtos.createdate = DateTime.Today;
            ShowRoomDtos.createby= User.Identity.GetUserName();
            _context.ShowRooms.Add(ShowRoom);
            _context.SaveChanges();

            ShowRoomDtos.id = ShowRoom.id;

            return Created(new Uri(Request.RequestUri + "/" + ShowRoomDtos.id), ShowRoomDtos);
        }

        [HttpPut]
        //PUT : /api/ShowRoom/{id}
        public IHttpActionResult EditShowRoom(int id, ShowRoomDto ShowRoomDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.ShowRooms.SingleOrDefault(c => c.name == ShowRoomDtos.name && c.id != ShowRoomDtos.id);
            if (isExist != null)
                return BadRequest();

            var ShowRoomInDb = _context.ShowRooms.SingleOrDefault(c => c.id == id);
            ShowRoomDtos.createby = User.Identity.GetUserName();
            Mapper.Map(ShowRoomDtos, ShowRoomInDb);
            _context.SaveChanges();

            return Ok(ShowRoomDtos);
        }

        [HttpDelete]
        //PUT : /api/Positions/{id}
        public IHttpActionResult DeleteShowRoom(int id)
        {

            var ShowRoomInDb = _context.ShowRooms.SingleOrDefault(c => c.id == id);
            if (ShowRoomInDb == null)
                return NotFound();
            _context.ShowRooms.Remove(ShowRoomInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
