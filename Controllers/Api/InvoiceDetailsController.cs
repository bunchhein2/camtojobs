﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class InvoiceDetailsController : ApiController
    {
        private ApplicationDbContext _context;

        public InvoiceDetailsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/InvoiceDetail")]
        public IHttpActionResult GetInvoicesDetail()
        {
            var getInvoiceDetails = from iv in _context.InvoiceDetails
                                    join i in _context.Invoices on iv.invoiceid equals i.id
                                    join e in _context.Employees on iv.employeeid equals e.id
                                    join p in _context.Products on iv.productid equals p.id
                                    join c in _context.Customers on i.customerid equals c.id
                                    join l in _context.Locations on iv.locationid equals l.id
                                    select new InvoiceDetailV
                                    {
                                        id = iv.id,
                                        invoiceid = iv.invoiceid,
                                        invoicedate = iv.invoicedate,
                                        locationid = iv.locationid,
                                        location = l.location,
                                        employeeid = iv.employeeid,
                                        employeename = e.name,
                                        productid = iv.productid,
                                        productname = p.productname,
                                        deliverytype = iv.deliverytype,
                                        receiverphone = iv.receiverphone,
                                        paidtype = iv.paidtype,
                                        price = iv.price,
                                        pricekh = iv.pricekh,
                                        carprice = iv.carprice,
                                        carpricekh = iv.carpricekh,
                                        shipprice = iv.shipprice,
                                        shippricekh = iv.shippricekh,
                                        status = iv.status,
                                        createby = iv.createby,
                                        createdate = iv.createdate,
                                        updateby = iv.updateby,
                                        updatedate = iv.updatedate,
                                        alreadymove = iv.alreadymove,
                                        datepickup = iv.datepickup,
                                        namepickup = iv.namepickup,
                                        paidtodriver = iv.paidtodriver,
                                        qtybox = iv.qtybox,
                                        barcode = iv.barcode,
                                        shipperoutid = iv.shipperoutid,
                                        shipperdate = iv.shipperdate,
                                        paidtoshop = iv.paidtoshop
                                    };
            return Ok(getInvoiceDetails);
        }
        [HttpGet]
        [Route("api/InvoiceDetail/{id}")]
        public IHttpActionResult GetInvoicesDetail(int id)
        {
            var getInvoiceDetailById = _context.InvoiceDetails.SingleOrDefault(c => c.id == id);
            return Ok(Mapper.Map<InvoiceDetail, InvoiceDetailDto>(getInvoiceDetailById));
        }
        [HttpGet]
        [Route("api/InvoiceDetail/{id}/{a}")]
        public IHttpActionResult GetInvoicesDetailByInvId(int id,string a)
        {
            var getInvoiceDetails = from iv in _context.InvoiceDetails
                                    join i in _context.Invoices on iv.invoiceid equals i.id
                                    join e in _context.Employees on iv.employeeid equals e.id
                                    join p in _context.Products on iv.productid equals p.id
                                    join c in _context.Customers on i.customerid equals c.id
                                    join l in _context.Locations on iv.locationid equals l.id
                                    where iv.invoiceid == id
                                    select new InvoiceDetailV
                                    {
                                        id = iv.id,
                                        invoiceid = iv.invoiceid,
                                        invoicedate = iv.invoicedate,
                                        locationid = iv.locationid,
                                        location = l.location,
                                        employeeid = iv.employeeid,
                                        employeename = e.name,
                                        productid = iv.productid,
                                        productname = p.productname,
                                        deliverytype = iv.deliverytype,
                                        receiverphone = iv.receiverphone,
                                        paidtype = iv.paidtype,
                                        price = iv.price,
                                        pricekh = iv.pricekh,
                                        carprice = iv.carprice,
                                        carpricekh = iv.carpricekh,
                                        shipprice = iv.shipprice,
                                        shippricekh = iv.shippricekh,
                                        status = iv.status,
                                        createby = iv.createby,
                                        createdate = iv.createdate,
                                        updateby = iv.updateby,
                                        updatedate = iv.updatedate,
                                        alreadymove = iv.alreadymove,
                                        datepickup = iv.datepickup,
                                        namepickup = iv.namepickup,
                                        paidtodriver = iv.paidtodriver,
                                        qtybox = iv.qtybox,
                                        barcode = iv.barcode,
                                        shipperoutid = iv.shipperoutid,
                                        shipperdate = iv.shipperdate,
                                        paidtoshop = iv.paidtoshop
                                    };
            return Ok(getInvoiceDetails);
            //var getInvoiceDetailById = _context.InvoiceDetails.Where(c => c.invoiceid == id);
            //return Ok(getInvoiceDetailById);
        }
        [HttpPost]
        [Route("api/InvoiceDetail")]
        public IHttpActionResult CreateInvoiceDetail(InvoiceDetailDto InvoiceDeatilDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var invoiceDetails = Mapper.Map<InvoiceDetailDto, InvoiceDetail>(InvoiceDeatilDtos);
            invoiceDetails.createby = User.Identity.GetUserName();
            invoiceDetails.createdate = DateTime.Today;
            invoiceDetails.invoicedate = DateTime.Today;
            invoiceDetails.alreadymove = false;
            invoiceDetails.paidtype = false;
            _context.InvoiceDetails.Add(invoiceDetails);
            _context.SaveChanges();

            InvoiceDeatilDtos.id = invoiceDetails.id;

            return Created(new Uri(Request.RequestUri + "/" + InvoiceDeatilDtos.id), InvoiceDeatilDtos);
        }
        [HttpPut]
        [Route("api/InvoiceDetail/{id}")]
        public IHttpActionResult EditInvoiceDetail(InvoiceDetailDto InvoiceDeatilDtos,int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var invoiceDetails = _context.InvoiceDetails.SingleOrDefault(c=>c.id==id);
            invoiceDetails.updateby = User.Identity.GetUserName();
            invoiceDetails.updatedate = DateTime.Today;
            Mapper.Map(InvoiceDeatilDtos, invoiceDetails);
            _context.SaveChanges();
            return Ok(InvoiceDeatilDtos);
        }
        [HttpDelete]
        [Route("api/InvoiceDetail/{id}")]
        public IHttpActionResult DeleteInvoiceDetail(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var invoiceDetails = _context.InvoiceDetails.SingleOrDefault(c => c.id == id);
            _context.InvoiceDetails.Remove(invoiceDetails);
            _context.SaveChanges();
            return Ok(new { });
        }
    }
}
