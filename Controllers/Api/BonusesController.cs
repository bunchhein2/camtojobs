﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CamTojobs.Controllers.Api
{
    public class BonusesController : ApiController
    {
        private ApplicationDbContext _context;

        public BonusesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        //Get : api/Bonuses

        public IHttpActionResult GetBonuses()
        {
            var getBonuses = from b in _context.Bonuses
                             join e in _context.Employees on b.employeeid equals e.id
                            select new
                            {
                                id = b.id,
                                date = b.date,
                                type=b.type,
                                employeeid=b.employeeid,
                                employeename = e.name,
                                amount = b.amount,
                                amountriel=b.amountriel,
                                note=b.note,
                                createby = b.createby,
                                createdate = b.createdate
                            };

            return Ok(getBonuses);
        }
        [HttpGet]
        //Get : api/Bonuses/{id}
        [Route("api/Bonuses/{id}")]
        public IHttpActionResult GetBonusesId(int id)
        {
            var getBonuses = _context.Bonuses.SingleOrDefault(c => c.id == id);

            return Ok(Mapper.Map<Bonus, BonusDto>(getBonuses));
        }
        [HttpGet]
        //Get : api/Bonuses/{id}
        [Route("api/BonusesByEmp/{id}/{a}")]
        public IHttpActionResult GetBonusesIdEmp(int id)
        {
            var getBonuses = _context.Bonuses.Where(c => c.employeeid == id);

            return Ok(getBonuses);
        }
        [HttpPost]
        //Get : api/Bonuses/{id}
        [Route("api/Bonuses")]
        public IHttpActionResult CreateBonuses(BonusDto bonusDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var bonuses = Mapper.Map<BonusDto, Bonus>(bonusDto);
            bonusDto.createdate = DateTime.Today;
            bonusDto.createby = User.Identity.GetUserName();
            _context.Bonuses.Add(bonuses);
            _context.SaveChanges();

            bonusDto.id = bonuses.id;

            return Created(new Uri(Request.RequestUri + "/" + bonusDto.id), bonusDto);
        }
        [HttpPut]
        //PUT : /api/Bonuses/{id}
        [Route("api/Bonuses/{id}")]
        public IHttpActionResult EditBonuses(int id, BonusDto bonusDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var bonusInDb = _context.Bonuses.SingleOrDefault(c => c.id == id);
            bonusDto.createby = User.Identity.GetUserName();
            Mapper.Map(bonusDto, bonusInDb);
            _context.SaveChanges();

            return Ok(bonusDto);
        }
        [HttpDelete]
        [Route("api/Bonuses/{id}")]

        public IHttpActionResult RemoveBonuses(int id)
        {
            var bonusInDb = _context.Bonuses.SingleOrDefault(c => c.id == id);
            _context.Bonuses.Remove(bonusInDb);
            _context.SaveChanges();
            return Ok(new { });
        }
    }
}
