﻿using AutoMapper;
using CamTojobs.Dtos;
using CamTojobs.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Data.Entity;

namespace CamTojobs.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        //[Route("api/Customers?departmentid={departmentid}")]
        //Get : api/Customers

        public IHttpActionResult GetCustomers(string showRoomId)
        {
            if (showRoomId == "All")
            {
                var getCustomers = _context.Customers
                                .Include(c => c.ShowRoom)
                                .Select(Mapper.Map<Customer, CustomerDto>);
                return Ok(getCustomers);
            }

            else
            {
                var getCustomers = _context.Customers
                                .Include(c => c.ShowRoom)
                                .Select(Mapper.Map<Customer, CustomerDto>)
                                .Where(c => c.showroomid == int.Parse(showRoomId));
                return Ok(getCustomers);
            }

        }


        [HttpGet]
        //[Route("api/Customers/{id}")]
        //Get : api/Customers{id}
        public IHttpActionResult GetCustomersID(int id)
        {
            var getCustomersById = _context.Customers.SingleOrDefault(c => c.id == id);

            if (getCustomersById == null)
                return NotFound();

            return Ok(Mapper.Map<Customer, CustomerDto>(getCustomersById));
        }
        [HttpPost]
        //[Route("api/Customers")]
        public IHttpActionResult CreateCustomers()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //var id = HttpContext.Current.Request.Form["Id"];
            var name = HttpContext.Current.Request.Form["name"];
            var sex = HttpContext.Current.Request.Form["sex"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var address = HttpContext.Current.Request.Form["address"];
            var identityno = HttpContext.Current.Request.Form["identityno"];
            var photo = HttpContext.Current.Request.Files["photo"];
            var showroomid = HttpContext.Current.Request.Form["showroomid"];
            var customertype = HttpContext.Current.Request.Form["customertype"];
            var createby = User.Identity.GetUserName();
            var createdate = DateTime.Today;

            var cusInDb = _context.Customers.SingleOrDefault(c => c.name == name && c.status == true);

            if (cusInDb != null)
                return BadRequest();

            string photoName = "";
            if (photo != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(photo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(photo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(photo.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                photo.SaveAs(fileSavePath);

            }

            var customerDto = new CustomerDto()
            {
                name = name,
                sex = sex,
                phone = phone,
                address = address,
                photo = photoName,
                identityno = identityno,
                customertype=customertype,
                showroomid = int.Parse(showroomid),
                createby = createby,
                createdate = createdate,
                status = true,
            };
            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            _context.Customers.Add(customer);
            _context.SaveChanges();

            customerDto.id = customer.id;

            return Created(new Uri(Request.RequestUri + "/" + customerDto.id), customerDto);
        }

        [HttpPut]
        //[Route("api/Customers/{id}")]
        //PUT : /api/Customers/{id}
        public IHttpActionResult EditCustomers(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var name = HttpContext.Current.Request.Form["name"];
            var sex = HttpContext.Current.Request.Form["sex"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var address = HttpContext.Current.Request.Form["address"];
            var identityno = HttpContext.Current.Request.Form["identityno"];
            var photo = HttpContext.Current.Request.Files["photo"];
            var showroomid = HttpContext.Current.Request.Form["showroomid"];
            var customertype = HttpContext.Current.Request.Form["customertype"];
            var createby = User.Identity.GetUserName();
            var createdate = DateTime.Today;

            var cusInDb = _context.Customers.SingleOrDefault(c => c.id == id/* && c.status == true*/);



            string photoName = "";
            if (photo != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(photo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(photo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(photo.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                photo.SaveAs(fileSavePath);

                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), cusInDb.photo);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
                var customerDto = new CustomerDto()
                {
                    id = id,
                    name = name,
                    sex = sex,
                    phone = phone,
                    address = address,
                    photo = photoName,
                    identityno = identityno,
                    customertype=customertype,
                    createby = createby,
                    createdate = createdate,
                    status = true,
                    showroomid = int.Parse(showroomid),
                };
                Mapper.Map(customerDto, cusInDb);
                _context.SaveChanges();
            }
            else
            {
                var customerDto = new CustomerDto()
                {
                    id = id,
                    name = name,
                    sex = sex,
                    phone = phone,
                    address = address,
                    photo=cusInDb.photo,
                    identityno = identityno,
                    customertype=customertype,
                    createby = createby,
                    createdate = createdate,
                    status = true,
                    showroomid = int.Parse(showroomid),
                };
                Mapper.Map(customerDto, cusInDb);
                _context.SaveChanges();
            }
            return Ok(new { });
        }

        [HttpDelete]
        [Route("api/Customers/{id}")]
        //PUT : /api/Customers/{id}
        public IHttpActionResult DeleteCustomers(int id)
        {
            var CustomersInDb = _context.Customers.SingleOrDefault(c => c.id == id);
            if (CustomersInDb == null)
                return NotFound();
            _context.Customers.Remove(CustomersInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
