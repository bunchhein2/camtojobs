﻿using CamTojobs.Models;
using CamTojobs.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CamTojobs.Controllers
{
    public class InvoicesController : Controller
    {
        ApplicationDbContext _context = new ApplicationDbContext();
        public InvoicesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Invoices
        public ActionResult Index()
        {
            var employeeVM = new EmployeeViewModel()
            {
                ShowRooms = _context.ShowRooms.ToList(),
                Customers=_context.Customers.ToList(),
                Locations=_context.Locations.ToList(),
                Products=_context.Products.ToList(),
                Employees=_context.Employees.ToList()
            };
            return View(employeeVM);
        }
    }
}