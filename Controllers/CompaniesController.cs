﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CamTojobs.Models;

namespace CamTojobs.Controllers
{
    public class CompaniesController : Controller
    {
        private ApplicationDbContext _context;

        public CompaniesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose(); 
        }

        // GET: Companies
        [Route("manage-company")]
        public ActionResult Index()
        {
            var conpanyType = _context.companyTypes.ToList();

            return View(conpanyType);
        }
    }
}