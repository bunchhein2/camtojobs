﻿using CamTojobs.Models;
using CamTojobs.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CamTojobs.Controllers
{
    public class EmployeesController : Controller
    {
        ApplicationDbContext _context = new ApplicationDbContext();
        public EmployeesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Employees
        public ActionResult Index()
        {
            var employeeVM = new EmployeeViewModel()
            {
                Departments = _context.Departments.ToList(),
                ShowRooms=_context.ShowRooms.ToList(),
                Positions=_context.Positions.ToList()
        };
            return View(employeeVM);
        }
    }
}