﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CamTojobs.Models;
using CamTojobs.ViewModel;

namespace CamTojobs.Controllers
{
    
    public class VacancysController : Controller
    {
        // GET: Vacancys
        ApplicationDbContext _context = new ApplicationDbContext();
        public VacancysController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();    
        }
        [Route("manage-vacancy")]
        public ActionResult Index()
        {
            var viewModel = new VancancyViewModel()
            {
                Companies = _context.companies.ToList().Where(c => c.IsDeleted == false),
                Categories = _context.Categories.ToList(),
                Positions = _context.Positions.ToList()
            };
            return View(viewModel);
        }
    }
}