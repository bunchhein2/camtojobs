﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class ShowRoomDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string owner { get; set; }
        public string phone { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public bool status { get; set; }
    }
}