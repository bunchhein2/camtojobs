﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class PositionDto
    {
        public int id { get; set; }
        [Required]
        [StringLength(100)]
        public string positionName { get; set; }
    }
}