﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CamTojobs.Models;

namespace CamTojobs.Dtos
{
    public class CompanyDtos
    {
        public int Id { get; set; }
        [require]
        [stringlength(255)]
        public string CompanyName { get; set; }
        [require]

        public int CompanyTypeId { get; set; }
        public CompanyType CompanyType { get; set; }
        [require]
        [stringlength(255)]
        public string ContactPerson { get; set; }
        [require]
        public int ContactPhone { get; set; }
        [stringlength(255)]
        public string Email { get; set; }
        [stringlength(255)]
        public string Address { get; set; }
        [require]
        public int PostLimitations { get; set; }
        public string Photo { get; set; }
        public bool IsDeleted { get; set; }

        public CompanyDtos()
        {
            PostLimitations = 0;
            IsDeleted = false;
        }

        
    }
}