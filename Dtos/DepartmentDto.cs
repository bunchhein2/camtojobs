﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class DepartmentDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}