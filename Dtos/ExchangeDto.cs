﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class ExchangeDto
    {
        [Key]
        public int rateid { get; set; }
        public DateTime? date { get; set; }
        public decimal rate { get; set; }
        public bool status { get; set; }
        public bool IsDeleted { get; set; }
        public ExchangeDto()
        {
            IsDeleted = false;
        }
    }
}