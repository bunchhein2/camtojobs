﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class ProductTypeDto
    {
        public int id { get; set; }
        public string producttypename { get; set; }
        public bool status { get; set; }
    }
}