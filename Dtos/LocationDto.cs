﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class LocationDto
    {
        public int id { get; set; }
        public string location { get; set; }
        public decimal price { get; set; }
        public bool status { get; set; }
    }
}