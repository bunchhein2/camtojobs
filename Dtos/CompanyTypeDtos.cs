﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CamTojobs.Models;

namespace CamTojobs.Dtos
{
    public class CompanyTypeDtos
    {
        public int Id { get; set; }
        [require]
        [stringlength(50)]
        public string Name { get; set; }
    }
}