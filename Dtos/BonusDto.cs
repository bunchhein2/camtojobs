﻿using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class BonusDto
    {
        public int id { get; set; }
        public DateTime? date { get; set; }
        public string type { get; set; }
        public int employeeid { get; set; }
        public Employee employee { get; set; }
        public decimal amount { get; set; }
        public decimal amountriel { get; set; }
        public string note { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}