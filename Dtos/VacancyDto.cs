﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CamTojobs.Models;

namespace CamTojobs.Dtos
{
    public class VacancyDto
    {
        public int Id { get; set; }
        [Required]
        public DateTime? PostingDate { get; set; }
        [Required]
        public DateTime? DeadLine { get; set; }
        [Required]
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        [Required]
        public int PositionId { get; set; }
        public Position Position { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        [Required]
        public int PositionAvailable { get; set; }
        [stringlength(255)]
        [Required]
        public string ContractType { get; set; }
        [Required]
        [stringlength(255)]
        public string Shcedule { get; set; }
        [Required]
        [stringlength(255)]
        public string Salary { get; set; }
        [Required]
        [stringlength(255)]
        public string ProductType { get; set; }
        [Required]
        [stringlength(1000)]
        public string Responsiblities { get; set; }
        [Required]
        [stringlength(1000)]
        public string Requirement { get; set; }
        [Required]
        [stringlength(1000)]
        public string ApplicationInformation { get; set; }
        [Required]
        [stringlength(255)]
        public string Status { get; set; }
    }
}