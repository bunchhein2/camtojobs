﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class CompanysDto
    {
        public int id { get; set; }
        [Required]
        [StringLength(100)]
        public string companyName { get; set; }
        [Required]
        [StringLength(100)]
        public string city { get; set; }
        [Required]
        [StringLength(100)]
        public string address { get; set; }
        [Required]
        [StringLength(100)]
        public string contact { get; set; }
        [StringLength(100)]
        public string email { get; set; }
    }
}