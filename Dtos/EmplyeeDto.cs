﻿using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class EmplyeeDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string namekh { get; set; }
        public string sex { get; set; }
        public string phone { get; set; }
        public DateTime? dob { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public int positionid { get; set; }
        public Position Position { get; set; }
        public string photo { get; set; }
        public string identityno { get; set; }
        public string shippertype { get; set; }
        public string vehicle { get; set; }
        public string plateno { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public bool status { get; set; }
        public int showroomid { get; set; }
        public ShowRoom ShowRoom { get; set; }
        public int departmentid { get; set; }
        public Department Department { get; set; }
        public decimal phonecard { get; set; }
        public decimal petoluem { get; set; }
        public decimal deliveryin { get; set; }
        public decimal deliveryout { get; set; }
    }
}