﻿using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class CustomerDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string sex { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string identityno { get; set; }
        public string photo { get; set; }
        public string customertype { get; set; }
        public bool status { get; set; }
        public int showroomid { get; set; }
        public ShowRoom ShowRoom { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}