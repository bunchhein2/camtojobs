﻿using CamTojobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CamTojobs.Dtos
{
    public class SalaryDto
    {
        public int id { get; set; }
        public DateTime? date { get; set; }
        public decimal amount { get; set; }
        public int employeeid { get; set; }
        public Employee employee { get; set; }

        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}