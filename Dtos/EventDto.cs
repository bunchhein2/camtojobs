﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CamTojobs.Models;

namespace CamTojobs.Dtos
{
    public class EventDto
    {
        public int id { get; set; }
        [Required]
        [stringlength(100)]
        public string event_name { get; set; }
        public DateTime event_date { get; set; }
        [Required]
        [stringlength(100)]
        public string place { get; set; }
        public bool status { get; set; }
    }
}