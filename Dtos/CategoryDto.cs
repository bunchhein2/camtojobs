﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CamTojobs.Models;

namespace CamTojobs.Dtos
{
    public class CategoryDto
    {
        public int id { get; set; }
        [Required]
        [stringlength(100)]
        public string categoryName { get; set; }
    }
}